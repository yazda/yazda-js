/*jshint node:true*/
module.exports = {
  name: 'abba',

  contentFor:        function(type, config) {
    if(type === 'head') {
      return '<script src="' + config.ABBA + '"></script>';
    }
  },
  isDevelopingAddon: function() {
    return true;
  }
};
