/*jshint node:true*/
module.exports = function(app) {
  var express = require('express');
  var friendshipsRouter = express.Router();

  friendshipsRouter.get('/incoming', function(req, res) {
    res.send({
      "page":        1,
      "total":       1,
      "page_count":  1,
      "friendships": [{
        "id":                53,
        "status":            "pending",
        "user":              {
          "id":                      7,
          "name":                    "Jack Stone",
          "location":                "Aurora CO, United States",
          "profile_image_url":       "http://dev.yazdaapp.com/users/7/avatar",
          "profile_image_thumb_url": "http://dev.yazdaapp.com/users/7/avatar?size=thumb",
          "banner_image_url":        "http://dev.yazdaapp.com/users/7/banner",
          "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/7/banner?size=thumb",
          "is_adventure_leader":     false,
          "following":               false
        },
        "friend":            {
          "id":                      1,
          "name":                    "Joe Lathey",
          "location":                "Golden, CO",
          "profile_image_url":       "http://dev.yazdaapp.com/users/1/avatar",
          "profile_image_thumb_url": "http://dev.yazdaapp.com/users/1/avatar?size=thumb",
          "banner_image_url":        "http://dev.yazdaapp.com/users/1/banner",
          "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/1/banner?size=thumb",
          "is_adventure_leader":     false,
          "following":               true
        },
        "created_at":        "2016-06-16T17:38:23.786Z",
        "created_at_epoch":  1466098703,
        "updated_at":        "2016-06-16T17:38:23.786Z",
        "updated_at_epoch":  1466098703,
        "accepted_at":       null,
        "accepted_at_epoch": 0,
        "rejected_at":       null,
        "rejected_at_epoch": 0
      }]
    });
  });

  friendshipsRouter.post('/', function(req, res) {
    res.status(201).end();
  });

  friendshipsRouter.get('/:id', function(req, res) {
    res.send({
      'friendships': {
        id: req.params.id
      }
    });
  });

  friendshipsRouter.put('/:id', function(req, res) {
    res.send({
      'friendships': {
        id: req.params.id
      }
    });
  });

  friendshipsRouter.delete('/:id', function(req, res) {
    res.status(204).end();
  });

  // The POST and PUT call will not contain a request body
  // because the body-parser is not included by default.
  // To use req.body, run:

  //    npm install --save-dev body-parser

  // After installing, you need to `use` the body-parser for
  // this mock uncommenting the following line:
  //
  //app.use('/api/friendships', require('body-parser').json());
  app.use('/friendships', friendshipsRouter);
};
