/*jshint node:true*/
module.exports = function(app) {
  var express = require('express');
  var invitesRouter = express.Router();

  invitesRouter.get('/pending', function(req, res) {
    res.send(
      {
        "page":       1,
        "total":      7,
        "page_count": 1,
        "invites":    [{
          "id":                33,
          "attending":         "pending",
          "accepted_at":       null,
          "accepted_at_epoch": 0,
          "rejected_at":       null,
          "rejected_at_epoch": 0,
          "created_at":        "2016-06-03T15:07:10.763Z",
          "created_at_epoch":  1464966430,
          "user":              {
            "id":                      2,
            "name":                    "Lisa Riley",
            "location":                "Lakewood CO, United States",
            "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
            "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
            "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
            "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
            "is_adventure_leader":     false,
            "following":               true
          },
          "adventure":         {
            "id":                  234,
            "adventure_type":      "biking",
            "reservation_limit":   4,
            "name":                "will it work",
            "description":         "asdfasdfasdf",
            "private":             false,
            "start_time_epoch":    1465452000,
            "start_time":          "2016-06-09T06:00:00.000Z",
            "end_time_epoch":      1466685000,
            "end_time":            "2016-06-23T12:30:00.000Z",
            "duration_in_minutes": 20550.0,
            "address":             "300 Pine St, Seattle, WA 98101, USA",
            "lat":                 47.611476,
            "lon":                 -122.338605,
            "created_at":          "2016-06-03T15:07:10.445Z",
            "created_at_epoch":    1464966430,
            "updated_at":          "2016-06-03T15:07:10.445Z",
            "updated_at_epoch":    1464966430,
            "skill_level":         null,
            "is_owner":            false,
            "is_invited":          true,
            "is_attending":        false,
            "has_responded":       false,
            "canceled":            false,
            "sponsored":           null,
            "invite":              {
              "id":                33,
              "attending":         "pending",
              "accepted_at":       null,
              "accepted_at_epoch": 0,
              "rejected_at":       null,
              "rejected_at_epoch": 0,
              "created_at":        "2016-06-03T15:07:10.763Z",
              "created_at_epoch":  1464966430,
              "user":              {
                "id":                      2,
                "name":                    "Lisa Riley",
                "location":                "Lakewood CO, United States",
                "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
                "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
                "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
                "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
                "is_adventure_leader":     false,
                "following":               true
              }
            },
            "owner":               {
              "id":                      1,
              "name":                    "Joe Lathey",
              "location":                "Golden, CO",
              "profile_image_url":       "http://dev.yazdaapp.com/users/1/avatar",
              "profile_image_thumb_url": "http://dev.yazdaapp.com/users/1/avatar?size=thumb",
              "banner_image_url":        "http://dev.yazdaapp.com/users/1/banner",
              "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/1/banner?size=thumb",
              "is_adventure_leader":     false,
              "following":               true
            }
          }
        }, {
          "id":                36,
          "attending":         "pending",
          "accepted_at":       null,
          "accepted_at_epoch": 0,
          "rejected_at":       null,
          "rejected_at_epoch": 0,
          "created_at":        "2016-06-03T15:20:11.811Z",
          "created_at_epoch":  1464967211,
          "user":              {
            "id":                      2,
            "name":                    "Lisa Riley",
            "location":                "Lakewood CO, United States",
            "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
            "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
            "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
            "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
            "is_adventure_leader":     false,
            "following":               true
          },
          "adventure":         {
            "id":                  236,
            "adventure_type":      "biking",
            "reservation_limit":   2,
            "name":                "testing",
            "description":         "asdfasdfasdf\n\nasdf",
            "private":             false,
            "start_time_epoch":    1465475400,
            "start_time":          "2016-06-09T12:30:00.000Z",
            "end_time_epoch":      1467305400,
            "end_time":            "2016-06-30T16:50:00.000Z",
            "duration_in_minutes": 30500.0,
            "address":             "830 Ford St, Llano, TX 78643, USA",
            "lat":                 30.7493024,
            "lon":                 -98.6758886,
            "created_at":          "2016-06-03T15:20:11.407Z",
            "created_at_epoch":    1464967211,
            "updated_at":          "2016-06-03T15:20:11.407Z",
            "updated_at_epoch":    1464967211,
            "skill_level":         null,
            "is_owner":            false,
            "is_invited":          true,
            "is_attending":        false,
            "has_responded":       false,
            "canceled":            false,
            "sponsored":           null,
            "invite":              {
              "id":                36,
              "attending":         "pending",
              "accepted_at":       null,
              "accepted_at_epoch": 0,
              "rejected_at":       null,
              "rejected_at_epoch": 0,
              "created_at":        "2016-06-03T15:20:11.811Z",
              "created_at_epoch":  1464967211,
              "user":              {
                "id":                      2,
                "name":                    "Lisa Riley",
                "location":                "Lakewood CO, United States",
                "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
                "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
                "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
                "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
                "is_adventure_leader":     false,
                "following":               true
              }
            },
            "owner":               {
              "id":                      1,
              "name":                    "Joe Lathey",
              "location":                "Golden, CO",
              "profile_image_url":       "http://dev.yazdaapp.com/users/1/avatar",
              "profile_image_thumb_url": "http://dev.yazdaapp.com/users/1/avatar?size=thumb",
              "banner_image_url":        "http://dev.yazdaapp.com/users/1/banner",
              "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/1/banner?size=thumb",
              "is_adventure_leader":     false,
              "following":               true
            }
          }
        }, {
          "id":                40,
          "attending":         "pending",
          "accepted_at":       null,
          "accepted_at_epoch": 0,
          "rejected_at":       null,
          "rejected_at_epoch": 0,
          "created_at":        "2016-06-03T18:41:16.545Z",
          "created_at_epoch":  1464979276,
          "user":              {
            "id":                      2,
            "name":                    "Lisa Riley",
            "location":                "Lakewood CO, United States",
            "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
            "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
            "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
            "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
            "is_adventure_leader":     false,
            "following":               true
          },
          "adventure":         {
            "id":                  237,
            "adventure_type":      "mountain_biking",
            "reservation_limit":   null,
            "name":                "Website ya!",
            "description":         null,
            "private":             false,
            "start_time_epoch":    1465546200,
            "start_time":          "2016-06-10T08:10:00.000Z",
            "end_time_epoch":      1466114100,
            "end_time":            "2016-06-16T21:55:00.000Z",
            "duration_in_minutes": 9465.0,
            "address":             "1000 S Rooney Rd, Morrison, CO 80465, USA",
            "lat":                 39.6962343,
            "lon":                 -105.1917493,
            "created_at":          "2016-06-03T18:41:16.079Z",
            "created_at_epoch":    1464979276,
            "updated_at":          "2016-06-09T13:27:14.766Z",
            "updated_at_epoch":    1465478834,
            "skill_level":         null,
            "is_owner":            false,
            "is_invited":          true,
            "is_attending":        false,
            "has_responded":       false,
            "canceled":            false,
            "sponsored":           true,
            "invite":              {
              "id":                40,
              "attending":         "pending",
              "accepted_at":       null,
              "accepted_at_epoch": 0,
              "rejected_at":       null,
              "rejected_at_epoch": 0,
              "created_at":        "2016-06-03T18:41:16.545Z",
              "created_at_epoch":  1464979276,
              "user":              {
                "id":                      2,
                "name":                    "Lisa Riley",
                "location":                "Lakewood CO, United States",
                "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
                "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
                "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
                "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
                "is_adventure_leader":     false,
                "following":               true
              }
            },
            "owner":               {
              "id":                      1,
              "name":                    "Joe Lathey",
              "location":                "Golden, CO",
              "profile_image_url":       "http://dev.yazdaapp.com/users/1/avatar",
              "profile_image_thumb_url": "http://dev.yazdaapp.com/users/1/avatar?size=thumb",
              "banner_image_url":        "http://dev.yazdaapp.com/users/1/banner",
              "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/1/banner?size=thumb",
              "is_adventure_leader":     false,
              "following":               true
            }
          }
        }, {
          "id":                5,
          "attending":         "pending",
          "accepted_at":       null,
          "accepted_at_epoch": 0,
          "rejected_at":       "2016-04-19T12:05:32.463Z",
          "rejected_at_epoch": 1461067532,
          "created_at":        "2016-03-24T20:07:32.886Z",
          "created_at_epoch":  1458850052,
          "user":              {
            "id":                      2,
            "name":                    "Lisa Riley",
            "location":                "Lakewood CO, United States",
            "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
            "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
            "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
            "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
            "is_adventure_leader":     false,
            "following":               true
          },
          "adventure":         {
            "id":                  7,
            "adventure_type":      "hiking",
            "reservation_limit":   null,
            "name":                "Going for a sweet hike",
            "description":         "http://google.com\n\nTesting\n\nnew lines\n\n- omg",
            "private":             false,
            "start_time_epoch":    1465930410,
            "start_time":          "2016-06-14T18:53:30.311Z",
            "end_time_epoch":      1466535210,
            "end_time":            "2016-06-21T18:53:30.315Z",
            "duration_in_minutes": 10080.0,
            "address":             "11390 W 76th Way, Arvada, CO 80005, USA",
            "lat":                 39.837708,
            "lon":                 -105.126464,
            "created_at":          "2016-03-24T20:07:32.670Z",
            "created_at_epoch":    1458850052,
            "updated_at":          "2016-05-31T19:15:19.956Z",
            "updated_at_epoch":    1464722119,
            "skill_level":         null,
            "is_owner":            false,
            "is_invited":          true,
            "is_attending":        false,
            "has_responded":       false,
            "canceled":            false,
            "sponsored":           true,
            "invite":              {
              "id":                5,
              "attending":         "pending",
              "accepted_at":       null,
              "accepted_at_epoch": 0,
              "rejected_at":       "2016-04-19T12:05:32.463Z",
              "rejected_at_epoch": 1461067532,
              "created_at":        "2016-03-24T20:07:32.886Z",
              "created_at_epoch":  1458850052,
              "user":              {
                "id":                      2,
                "name":                    "Lisa Riley",
                "location":                "Lakewood CO, United States",
                "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
                "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
                "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
                "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
                "is_adventure_leader":     false,
                "following":               true
              }
            },
            "owner":               {
              "id":                      1,
              "name":                    "Joe Lathey",
              "location":                "Golden, CO",
              "profile_image_url":       "http://dev.yazdaapp.com/users/1/avatar",
              "profile_image_thumb_url": "http://dev.yazdaapp.com/users/1/avatar?size=thumb",
              "banner_image_url":        "http://dev.yazdaapp.com/users/1/banner",
              "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/1/banner?size=thumb",
              "is_adventure_leader":     false,
              "following":               true
            }
          }
        }, {
          "id":                23,
          "attending":         "pending",
          "accepted_at":       null,
          "accepted_at_epoch": 0,
          "rejected_at":       "2016-04-19T12:16:05.137Z",
          "rejected_at_epoch": 1461068165,
          "created_at":        "2016-04-19T12:16:05.219Z",
          "created_at_epoch":  1461068165,
          "user":              {
            "id":                      2,
            "name":                    "Lisa Riley",
            "location":                "Lakewood CO, United States",
            "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
            "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
            "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
            "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
            "is_adventure_leader":     false,
            "following":               true
          },
          "adventure":         {
            "id":                  8,
            "adventure_type":      "biking",
            "reservation_limit":   null,
            "name":                "Road Riding for some fun",
            "description":         "here is a description. with a link \\n\\n http://google.com",
            "private":             false,
            "start_time_epoch":    1465930410,
            "start_time":          "2016-06-14T18:53:30.311Z",
            "end_time_epoch":      1466535210,
            "end_time":            "2016-06-21T18:53:30.315Z",
            "duration_in_minutes": 10080.0,
            "address":             "9261 S Crestmore Way, Littleton, CO 80126, USA",
            "lat":                 39.548058,
            "lon":                 -104.973404,
            "created_at":          "2016-03-25T14:30:54.350Z",
            "created_at_epoch":    1458916254,
            "updated_at":          "2016-05-31T22:03:23.344Z",
            "updated_at_epoch":    1464732203,
            "skill_level":         null,
            "is_owner":            false,
            "is_invited":          true,
            "is_attending":        false,
            "has_responded":       false,
            "canceled":            false,
            "sponsored":           null,
            "invite":              {
              "id":                23,
              "attending":         "pending",
              "accepted_at":       null,
              "accepted_at_epoch": 0,
              "rejected_at":       "2016-04-19T12:16:05.137Z",
              "rejected_at_epoch": 1461068165,
              "created_at":        "2016-04-19T12:16:05.219Z",
              "created_at_epoch":  1461068165,
              "user":              {
                "id":                      2,
                "name":                    "Lisa Riley",
                "location":                "Lakewood CO, United States",
                "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
                "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
                "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
                "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
                "is_adventure_leader":     false,
                "following":               true
              }
            },
            "owner":               {
              "id":                      8,
              "name":                    "Test 7",
              "location":                null,
              "profile_image_url":       "http://dev.yazdaapp.com/users/8/avatar",
              "profile_image_thumb_url": "http://dev.yazdaapp.com/users/8/avatar?size=thumb",
              "banner_image_url":        "http://dev.yazdaapp.com/users/8/banner",
              "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/8/banner?size=thumb",
              "is_adventure_leader":     false,
              "following":               false
            }
          }
        }, {
          "id":                4,
          "attending":         "pending",
          "accepted_at":       null,
          "accepted_at_epoch": 0,
          "rejected_at":       null,
          "rejected_at_epoch": 0,
          "created_at":        "2016-03-22T18:03:57.522Z",
          "created_at_epoch":  1458669837,
          "user":              {
            "id":                      2,
            "name":                    "Lisa Riley",
            "location":                "Lakewood CO, United States",
            "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
            "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
            "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
            "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
            "is_adventure_leader":     false,
            "following":               true
          },
          "adventure":         {
            "id":                  6,
            "adventure_type":      "mountain_biking",
            "reservation_limit":   null,
            "name":                "Going for a sick road ride OMG yes super long title let's see if it works",
            "description":         null,
            "private":             false,
            "start_time_epoch":    1465930410,
            "start_time":          "2016-06-14T18:53:30.311Z",
            "end_time_epoch":      1466535210,
            "end_time":            "2016-06-21T18:53:30.315Z",
            "duration_in_minutes": 10080.0,
            "address":             "1641 Wadsworth Blvd, Lakewood, CO 80232, USA",
            "lat":                 39.6870789,
            "lon":                 -105.0818738,
            "created_at":          "2016-03-22T18:03:57.415Z",
            "created_at_epoch":    1458669837,
            "updated_at":          "2016-05-31T18:53:43.667Z",
            "updated_at_epoch":    1464720823,
            "skill_level":         null,
            "is_owner":            false,
            "is_invited":          true,
            "is_attending":        false,
            "has_responded":       false,
            "canceled":            false,
            "sponsored":           true,
            "invite":              {
              "id":                4,
              "attending":         "pending",
              "accepted_at":       null,
              "accepted_at_epoch": 0,
              "rejected_at":       null,
              "rejected_at_epoch": 0,
              "created_at":        "2016-03-22T18:03:57.522Z",
              "created_at_epoch":  1458669837,
              "user":              {
                "id":                      2,
                "name":                    "Lisa Riley",
                "location":                "Lakewood CO, United States",
                "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
                "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
                "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
                "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
                "is_adventure_leader":     false,
                "following":               true
              }
            },
            "owner":               {
              "id":                      1,
              "name":                    "Joe Lathey",
              "location":                "Golden, CO",
              "profile_image_url":       "http://dev.yazdaapp.com/users/1/avatar",
              "profile_image_thumb_url": "http://dev.yazdaapp.com/users/1/avatar?size=thumb",
              "banner_image_url":        "http://dev.yazdaapp.com/users/1/banner",
              "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/1/banner?size=thumb",
              "is_adventure_leader":     false,
              "following":               true
            }
          }
        }, {
          "id":                35,
          "attending":         "pending",
          "accepted_at":       null,
          "accepted_at_epoch": 0,
          "rejected_at":       null,
          "rejected_at_epoch": 0,
          "created_at":        "2016-06-03T15:15:05.173Z",
          "created_at_epoch":  1464966905,
          "user":              {
            "id":                      2,
            "name":                    "Lisa Riley",
            "location":                "Lakewood CO, United States",
            "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
            "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
            "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
            "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
            "is_adventure_leader":     false,
            "following":               true
          },
          "adventure":         {
            "id":                  235,
            "adventure_type":      "mountain_biking",
            "reservation_limit":   12,
            "name":                "testing",
            "description":         null,
            "private":             false,
            "start_time_epoch":    1466080200,
            "start_time":          "2016-06-16T12:30:00.000Z",
            "end_time_epoch":      1466700600,
            "end_time":            "2016-06-23T16:50:00.000Z",
            "duration_in_minutes": 10340.0,
            "address":             "2-186 S Rooney Rd, Golden, CO 80401, USA",
            "lat":                 39.7137788,
            "lon":                 -105.1977812,
            "created_at":          "2016-06-03T15:15:04.870Z",
            "created_at_epoch":    1464966904,
            "updated_at":          "2016-06-03T15:15:04.870Z",
            "updated_at_epoch":    1464966904,
            "skill_level":         null,
            "is_owner":            false,
            "is_invited":          true,
            "is_attending":        false,
            "has_responded":       false,
            "canceled":            false,
            "sponsored":           null,
            "invite":              {
              "id":                35,
              "attending":         "pending",
              "accepted_at":       null,
              "accepted_at_epoch": 0,
              "rejected_at":       null,
              "rejected_at_epoch": 0,
              "created_at":        "2016-06-03T15:15:05.173Z",
              "created_at_epoch":  1464966905,
              "user":              {
                "id":                      2,
                "name":                    "Lisa Riley",
                "location":                "Lakewood CO, United States",
                "profile_image_url":       "http://dev.yazdaapp.com/users/2/avatar",
                "profile_image_thumb_url": "http://dev.yazdaapp.com/users/2/avatar?size=thumb",
                "banner_image_url":        "http://dev.yazdaapp.com/users/2/banner",
                "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/2/banner?size=thumb",
                "is_adventure_leader":     false,
                "following":               true
              }
            },
            "owner":               {
              "id":                      1,
              "name":                    "Joe Lathey",
              "location":                "Golden, CO",
              "profile_image_url":       "http://dev.yazdaapp.com/users/1/avatar",
              "profile_image_thumb_url": "http://dev.yazdaapp.com/users/1/avatar?size=thumb",
              "banner_image_url":        "http://dev.yazdaapp.com/users/1/banner",
              "banner_image_thumb_url":  "http://dev.yazdaapp.com/users/1/banner?size=thumb",
              "is_adventure_leader":     false,
              "following":               true
            }
          }
        }]
      });
  });

  // TODO
  invitesRouter.post('/', function(req, res) {
    res.status(201).end();
  });

  invitesRouter.get('/:id', function(req, res) {
    res.send({
      'invites': {
        id: req.params.id
      }
    });
  });

  invitesRouter.put('/:id', function(req, res) {
    res.send({
      'invites': {
        id: req.params.id
      }
    });
  });

  invitesRouter.delete('/:id', function(req, res) {
    res.status(204).end();
  });

  // The POST and PUT call will not contain a request body
  // because the body-parser is not included by default.
  // To use req.body, run:

  //    npm install --save-dev body-parser

  // After installing, you need to `use` the body-parser for
  // this mock uncommenting the following line:
  //
  //app.use('/api/invites', require('body-parser').json());
  app.use('/invites', invitesRouter);
};
