import Ember from 'ember';
import AsyncButton from 'ember-async-button/components/async-button';

const { get, computed, getWithDefault } = Ember;

AsyncButton.reopen({
  pending: '<div class="loading-btn sm">&nbsp;<span>Please Wait...</span></div>',
  text: computed('textState', 'default', 'pending', 'resolved', 'fulfilled', 'rejected', function() {
    let value = getWithDefault(this, this.textState, get(this, 'default'));

    return Ember.String.htmlSafe(value);
  }),
});
