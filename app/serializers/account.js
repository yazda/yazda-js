import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    payload.account = payload.user;
    delete payload.user;

    return this._super(store, primaryModelClass, payload, id, requestType);
  }
});
