import Ember from 'ember';
import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  extractMeta: function(store, typeClass, payload) {
    if(payload && payload.hasOwnProperty('page')) {
      let meta = {
        page:       payload.page,
        page_count: payload.page_count,
        total:      payload.total
      };

      delete payload.page;
      delete payload.page_count;
      delete payload.total;
      return meta;
    }
  },
  serializeIntoHash(hash, typeClass, snapshot, options) {
    Ember.merge(hash, this.serialize(snapshot, options));
  }
});
