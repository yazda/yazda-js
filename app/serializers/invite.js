import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  serialize() {
    var json = this._super(...arguments);

    if(json.adventure) {
      json.adventure_id = json.adventure.id;
    }

    return json;
  }
});
