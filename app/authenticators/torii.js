import Ember from 'ember';
import Torii from 'ember-simple-auth/authenticators/torii';

const {inject: {service}} = Ember;

export default Torii.extend({
  torii: service(),
  ajax:  service(),

  authenticate() {
    const ajax = this.get('ajax');

    return this._super(...arguments).then((data) => {
      return ajax.request('/v1/oauth/' + data.provider, {
        type:     'GET',
        dataType: 'json',
        data:     {'auth_code': data.authorizationCode}
      }).then((response) => {
          return {
            access_token: response.accessToken,
            provider:     data.provider
          };
        },
        (err) => {
          console.log(err);
        });
    });
  }
});
