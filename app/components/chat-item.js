import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Component.extend({
  sessionAccount: service('session-account'),

  getSide: function() {
    let msgUser = this.get('message.user');
    let me = this.get('sessionAccount');

    if(Number.parseInt(me.get('account.id')) === msgUser.id) {
      return 'right';
    }else {
      return 'left';
    }
  }.property('message')
});
