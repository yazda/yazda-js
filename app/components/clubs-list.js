import Ember from 'ember';

export default Ember.Component.extend({
  store:   Ember.inject.service(),
  loading: true,

  didInsertElement() {
    let self = this;
    let store = self.get('store');

    store
        .query('club', {user_id: this.get('userId')})
        .then((clubs) => {
          if(!(self.get('isDestroyed') || self.get('isDestroying'))) {
            self.set('clubs', clubs);
          }
        }).finally(function() {
      if(!(self.get('isDestroyed') || self.get('isDestroying'))) {
        self.set('loading', false);
      }

      return arguments;
    });
  }
});
