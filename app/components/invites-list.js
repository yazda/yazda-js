import Ember from 'ember';

export default Ember.Component.extend({
  store:          Ember.inject.service(),
  loading:        true,
  invitesLimited: Ember.computed('invites', function() {
    return this.get('invites').slice(0, 3);
  }),
  didInsertElement() {
    var self = this;
    var store = self.get('store');

    store.query('invite', {endpoint: 'pending'})
        .then((invites) => {
          if(!(self.get('isDestroyed') || self.get('isDestroying'))) {
            self.set('invites', invites);
          }
        }).finally(function() {
      if(!(self.get('isDestroyed') || self.get('isDestroying'))) {
        self.set('loading', false);
      }

      return arguments;
    });
  }
});
