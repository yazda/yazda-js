import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Component.extend({
  session:        service('session'),
  sessionAccount: service('session-account'),
  websocket:      service('yazda-websockets'),
  routing:        service('-routing'),

  canCreateClub:      function() {
    return this.get('routing.currentRouteName') === 'clubs.show';
  }.property('routing.currentRouteName'),
  canCreateAdventure: function() {
    var ret = true;

    switch(this.get('routing.currentRouteName')) {
      case 'adventures.new':
      case 'adventures.edit':
      case 'clubs.edit':
      case  'profile.edit':
        ret = false;
    }

    return ret;
  }.property('routing.currentRouteName'),
  canNavProfile:      function() {
    var ret = true;

    switch(this.get('routing.currentRouteName')) {
      case 'adventures.new':
      case 'adventures.edit':
      case 'clubs.show':
      case 'clubs.edit':
      case  'profile.index':
      case  'profile.edit':
        ret = false;
    }

    return ret;
  }.property('routing.currentRouteName'),
  canEditProfile:     function() {
    return this.get('routing.currentRouteName') === 'profile.index';
  }.property('routing.currentRouteName'),
  canCancel:          function() {
    var ret = false;

    switch(this.get('routing.currentRouteName')) {
      case  'profile.edit':
      case  'clubs.edit':
      case 'adventures.new':
      case 'adventures.edit':
        ret = true;
    }

    return ret;
  }.property('routing.currentRouteName'),
  actions:            {
    login() {
      this.sendAction('onLogin');
    },

    logout() {
      this.get('session').invalidate();
      this.get('websocket').disconnectFromSocket();
    }
  }
});
