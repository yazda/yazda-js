import Ember from 'ember';

export default Ember.Component.extend({
  flashMessages: Ember.inject.service(),

  mountainBikingSelected: Ember.computed('user.views_mountain_biking', function() {
    return this.get('user.views_mountain_biking') ? 'select-active' : '';
  }),
  bikingSelected:         Ember.computed('user.views_biking', function() {
    return this.get('user.views_biking') ? 'select-active' : '';
  }),
  hikingSelected:         Ember.computed('user.views_hiking', function() {
    return this.get('user.views_hiking') ? 'select-active' : '';
  }),
  bannerChanged:          function() {
    var self = this;
    var file = document.getElementById('backgroundFileUpload');

    if(file && file.files.length > 0) {
      var banner = file.files[0];

      banner.convertToBase64(function(base) {
        const flashMessages = self.get('flashMessages');

        if(base) {
          self.set('uploading', true);
          var user = self.get('user');

          user.set('banner', base);

          user.uploadBanner()
              .then(() => {
                flashMessages.success('Banner uploaded successfully');
                user.set('banner_image_url',
                    user.get('banner_image_url') + '?time=' + new Date());
              }).catch(function(error) {
            _.each(error.errors, function(e) {
              flashMessages.alert(e.detail);
            });
          })
              .finally(() => {
                user.set('banner', null);
                self.set('uploading', false);
              });
        }
      });
    }
  }.observes('banner'),

  avatarChanged: function() {
    const flashMessages = this.get('flashMessages');
    var self = this;
    var file = document.getElementById('avatarFileUpload');

    if(file && file.files.length > 0) {
      file.files[0].convertToBase64(function(base) {
        if(base) {
          self.set('uploading', true);

          var user = self.get('user');

          user.set('avatar', base);

          user.uploadAvatar()
              .then(() => {
                flashMessages.success('Avatar uploaded successfully');
                user.set('profile_image_url',
                    user.get('profile_image_url') + '?time=' + new Date());
              }).catch(function(error) {
            _.each(error.errors, function(e) {
              flashMessages.alert(e.detail);
            });
          })
              .finally(() => {
                user.set('avatar', null);
                self.set('uploading', false);
              });
        }
      });
    }

  }.observes('avatar'),

  actions: {
    selectAdventureType(type) {
      this.set('user.views_' + type, !this.get('user.views_' + type));
    },
    save() {
      let self = this;
      let user = this.get('user');

      user.set('location', this.$('#edit-location input').val());
      return user
          .postSettings()
          .then(function(user) {
            self.get('onSave')(user.get('id'));
          });
    }
  }
});
