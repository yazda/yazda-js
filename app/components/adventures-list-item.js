import Ember from 'ember';
import AdventureMixin from '../mixins/adventure';

export default Ember.Component.extend(AdventureMixin, {
  classNames:        ['item', 'adventure-item'],
  tagName:           'article',
  limitedAttendings: Ember.computed('model.attendings', function() {
    return this.get('model.attendings').slice(0, 4);
  }),

  skillLevel: Ember.computed('model.skill_level', function() {
    let skillLevel = this.get('model.skill_level');

    switch(skillLevel) {
      case 1:
        return 'easiest';
      case 2:
        return 'easy';
      case 3:
        if('mountain_biking' === this.get('model.adventure_type')) {
          return 'intermediate';
        }

        return 'medium';
      case 4:
        return 'hard';
      case 5:
        return 'hardest';
      default:
        return '';
    }
  }),

  adventureType: Ember.computed('model.adventure_type', function() {
    return this.get('model.adventure_type').replace('_', '-');
  })
});
