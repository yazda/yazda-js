import Ember from 'ember';

export default Ember.TextField.extend({
  tagName: 'input',
  _picker: null,

  modelChangedValue: Ember.observer('value', function() {
    var picker = this.get('_picker');
    if(picker) {
      picker.fdatepicker('update', this.get('value'));
    }
  }),
  didInsertElement() {
    var picker = this.$().fdatepicker({
      format:                   'yyyy-mm-dd hh:ii',
      disableDblClickSelection: false,
      language:                 'en',
      pickTime:                 true
    });

    this.set('_picker', picker);
  }
});
