import Ember from 'ember';

export default Ember.Component.extend({
  flashMessages: Ember.inject.service(),

  bannerChanged: function() {
    var self = this;
    var file = document.getElementById('backgroundFileUpload');

    if(file && file.files.length > 0) {
      file.files[0].convertToBase64(function(base) {
        if(base) {
          const flashMessages = self.get('flashMessages');
          self.set('uploading', true);
          var club = self.get('club');

          club.set('banner', base);

          club.uploadBanner()
              .then(() => {
                club.set('banner_image_url',
                    club.get('banner_image_url') + '?time=' + Math.random());
                flashMessages.success('Banner uploaded!');
              }).catch(function(error) {
                _.each(error.errors, function(e) {
                  flashMessages.alert(e.detail);
                });
              })
              .finally(() => {
                club.set('banner', null);
                self.set('uploading', false);
              });
        }
      });
    }
  }.observes('banner'),

  waiverChanged: function() {
    var self = this;
    var file = document.getElementById('waiverFileUpload');

    if(file && file.files.length > 0) {
      file.files[0].convertToBase64(function(base) {
        if(base) {
          const flashMessages = self.get('flashMessages');
          let club = self.get('club');

          self.set('uploading', true);

          club.set('waiver', base);

          club.uploadWaiver()
              .then(() => {
                flashMessages.success('Waiver uploaded!');
              }).catch(function(error) {
                _.each(error.errors, function(e) {
                  flashMessages.alert(e.detail);
                });
              })
              .finally(() => {
                self.set('uploading', false);
              });
        }
      });
    }
  }.observes('waiver'),

  avatarChanged: function() {
    var self = this;
    var file = document.getElementById('avatarFileUpload');

    if(file && file.files.length > 0) {
      file.files[0].convertToBase64(function(base) {
        if(base) {
          const flashMessages = self.get('flashMessages');
          var club = self.get('club');

          self.set('uploading', true);

          club.set('avatar', base);

          club.uploadAvatar()
              .then(() => {
                flashMessages.success('Profile image updated!');

                club.set('profile_image_url',
                    club.get('profile_image_url') + '?time=' + Math.random());
              }).catch(function(error) {
                _.each(error.errors, function(e) {
                  flashMessages.alert(e.detail);
                });
              })
              .finally(() => {
                club.set('avatar', null);
                self.set('uploading', false);
              });
        }
      });
    }
  }.observes('avatar'),

  actions: {
    save() {
      let self = this;
      var club = this.get('club');

      club.set('location', this.$('#edit-location input').val());

      return club.save()
          .then(function(club) {
            self.get('onSave')(club.get('id'));
          });
    }
  }
});
