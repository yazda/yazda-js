import Ember from 'ember';

export default Ember.Component.extend({
  session:       Ember.inject.service(),
  flashMessages: Ember.inject.service(),

  canEdit:                 Ember.computed('club.role', function() {
    let role = this.get('club.role');

    return role === 'admin' || role === 'owner';
  }),
  adventureInviteObserver: Ember.observer('club.adventure_invite', function(adventure_invite) {
    const flashMessages = this.get('flashMessages');
    let club = this.get('club');
    let self = this;

    return club.updateNotification()
        .then(function() {
          flashMessages.success('Notification preferences updated');
        }).catch(() => {
          flashMessages.alert('We encountered an error. Please try again.');
        });
  }),
  actions:                 {
    joinClub() {
      const flashMessages = this.get('flashMessages');

      if(this.get('session.isAuthenticated')) {
        var club = this.get('club');
        var self = this;

        return club
            .join()
            .then(function() {
              flashMessages.success("You've joined " + self.get('club.name'));
              club.set('is_member', true);
            }).catch(() => {
              flashMessages.alert('We encountered an error. Please try again.');
            });
      } else {
        this.router.transitionTo('login');
      }
    },
    leaveClub() {
      const flashMessages = this.get('flashMessages');

      if(this.get('session.isAuthenticated')) {
        var club = this.get('club');
        var self = this;

        return club
            .leave()
            .then(function() {
              flashMessages.success("You've left " + self.get('club.name'));
              club.set('is_member', false);
            }).catch(() => {
              flashMessages.alert('We encountered an error. Please try again.');
            });
      } else {
        this.router.transitionTo('login');
      }
    },
    openPanel1() {
      this.$('#user-show-tabs').foundation('selectTab', 'panel1');
    },
    openPanel2() {
      this.$('#user-show-tabs').foundation('selectTab', 'panel2');
    }
  }
});
