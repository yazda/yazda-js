import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Component.extend({
  classNames:     ['item', 'item-friend', 'item-messages', 'message-select'],
  tagName:        'article',
  sessionAccount: service('session-account'),

  title: function() {
    let conversation = this.get('conversation');
    let id = Number.parseInt(this.get('sessionAccount.account.id'));

    if(conversation && id) {
      return conversation.sentTo
          .filter((val) => {
            return val.id !== id;
          })
          .map((val) => val.name)
          .join(', ')
          .replace(/,([^,]*)$/, ' &$1');
    } else {
      return '';
    }
  }.property('conversation.sentTo'),

  firstThree: function() {
    let conversation = this.get('conversation');
    let id = Number.parseInt(this.get('sessionAccount.account.id'));

    if(conversation && id) {
      return conversation.sentTo
          .filter((val) => {
            return val.id !== id;
          })
          .slice(0, 2);
    } else {
      return [];
    }
  }.property('conversation.sentTo'),

  imageClass: function() {
    let length = this.get('firstThree').length;

    switch(length) {
      case 1:
        return 'thumb-user';
      case 2:
        return 'two';
      case 3:
        return 'three';
    }
  }.property('firstThree')
});
