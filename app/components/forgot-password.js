import Ember from 'ember';
import $ from 'jquery';

const {service} = Ember.inject;

export default Ember.Component.extend({
  ajax:    service(),
  actions: {
    sendResetPassword() {
      let self = this;
      let ajax = this.get('ajax');

      return ajax.request('/reset_password', {
        type:     'POST',
        dataType: 'text',
        data:     {email: this.get('email')}
      }).then(() => {
        $('#passwordModal').foundation('close');
        self.set('sentMessage', true);
      });
    }
  }
});
