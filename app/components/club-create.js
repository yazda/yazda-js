import Ember from 'ember';

export default Ember.Component.extend({
  flashMessages: Ember.inject.service(),

  actions: {
    save() {
      const flashMessages = this.get('flashMessages');
      let self = this;
      let club = this.get('club');

      club.set('location', this.$('#create-location input').val());

      return club.save()
        .then(function(club) {
          self.get('onSave')(club.get('id'));
        }, function() {
          flashMessages.alert('There was an error saving your club.');
        });
    }
  }
});
