import Ember from 'ember';
import PagingMixin from '../mixins/paging';

// TODO display selected correctly with pagination
export default Ember.Component.extend(PagingMixin, {
  classNames:          ['item-list', 'friends'],
  store:               Ember.inject.service(),
  loading:             true,
  userAndClubObserver: Ember.observer('userId', 'clubId', function() {
    this.set('loading', true);
    Ember.run.once(this, 'getModels');
  }),
  getModels:           function() {
    var self = this;
    var store = self.get('store');
    var rsvp;

    let userId = self.get('userId');
    let clubId = self.get('clubId');

    if(userId) {
      rsvp = self.set('users',
        store.query('friend', {
          userId:    userId,
          page:      this.get('page'),
          page_size: this.get('page_size')
        }));
    } else if(clubId) {
      rsvp = self.set('users',
        store.query('member', {
          clubId:    clubId,
          page:      this.get('page'),
          page_size: this.get('page_size')
        }));
    }

    rsvp
      .then((users) => {
        self.set('page_count', users.get('meta.page_count'));
        self.set('total', users.get('meta.total'));
      })
      .finally(function() {
        self.set('loading', false);
        return arguments;
      });
  }.on('didInsertElement'),
  actions:             {
    selectUser(userId) {
      let self = this;

      return new Ember.RSVP.Promise(function(resolve, reject) {
        if(self.has('onSelect')) {
          resolve(self.get('onSelect')(userId));
        } else {
          reject('onSelect not set');
        }
      });
    }
  }
});
