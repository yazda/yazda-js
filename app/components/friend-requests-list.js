import Ember from 'ember';

export default Ember.Component.extend({
  store:           Ember.inject.service(),
  loading:         true,
  requestsLimited: Ember.computed('requests.[]', function() {
    return this.get('requests').slice(0, 3);
  }),
  didInsertElement() {
    var self = this;
    var store = self.get('store');
    store
      .query('friendship', {endpoint: 'incoming'})
      .then((friendships) => {
        if(!(self.get('isDestroyed') || self.get('isDestroying'))) {
          self.set('requests', friendships);
        }
      }).finally(function() {
      if(!(self.get('isDestroyed') || self.get('isDestroying'))) {
        self.set('loading', false);
      }

      return arguments;
    });
  }
});
