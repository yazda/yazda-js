import Ember from 'ember';

export default Ember.Component.extend({
  displayError: false,
  actions:      {
    ya() {
      let self = this;
      let friendship = this.get('friendship');

      friendship.accept().then(function() {
        friendship.unloadRecord();
      }).catch(function() {
        self.set('displayError', true);
      });
    },
    na() {
      let self = this;
      let friendship = this.get('friendship');

      friendship.reject().then(function() {
        friendship.unloadRecord();
      }).catch(() => {
        self.set('displayError', true);
      });
    }
  }
});
