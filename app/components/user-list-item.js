import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['large-6', 'columns'],
  tagName:    'article',
  selected:   false,

  selectedClass: Ember.computed('selected', function() {
    return this.get('selected') ? 'friend-select-active' : '';
  }),
  click() {
    let self = this;
    const rsvp = this.get('onSelect')();

    rsvp.then(function(val) {
      self.set('selected', val);
    });
  }
});
