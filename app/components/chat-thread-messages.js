import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Component.extend({
  websocket: service('yazda-websockets'),
  sessionAccount: service('session-account'),
  adventure: null,
  msg:       '',

  // TODO handle changing adventures
  didInsertElement() {
    this._super(...arguments);

    if(this.get('adventure')) {
      let websocket = this.get('websocket');

      websocket.getAdventureMessages(this.get('adventure.id'));
    }

    this.scrollToBottom();
  },

  willDestroyElement() {
    this._super(...arguments);

    if(this.get('adventure')) {
      let websocket = this.get('websocket');

      websocket.removeAdventureMessages(this.get('adventure.id'));
    }

    this.scrollToBottom();
  },

  detectAdventure: function() {
    if(this.get('adventure')) {
      let websocket = this.get('websocket');

      websocket.getAdventureMessages(this.get('adventure.id'));
    }

    this.scrollToBottom();
  }.observes('adventure'),

  disabled: function() {
    return !this.get('msg').trim().length;
  }.property('msg'),

  scrollToBottom: function() {
    setTimeout(() => {
      let obj = $('.chat-show-content');

      if(obj && obj.length) {
        obj.animate({scrollTop: obj[0].scrollHeight}, 500);
      }
    }, 300);
  }.observes('websocket.messages', 'websocket.adventureMessages'),

  title:function() {
    let id = Number.parseInt(this.get('sessionAccount.account.id'));
    let messages = this.get('websocket.messages');

    if(messages && messages.length) {
      return messages[0].sentTo
          .filter((val) => {
            return val.id !== id;
          })
          .map((val) => val.name)
          .join(', ')
          .replace(/,([^,]*)$/, ' &$1');
    }else{
      return 'New Chat';
    }
  }.property('websocket.messages.[]'),

  actions: {
    open(url) {
      window.open(url, '_blank');
    },

    scroll: function() {
      this.scrollToBottom();
    },

    markRead: function() {
      let websocket = this.get('websocket');
// TODO conversations
      websocket.markAdventuresRead(this.get('adventure.id'));
    },

    sendMessage() {
      let websocket = this.get('websocket');

      if(this.get('adventure')) {
        websocket.sendMessageToAdventurers(this.get('adventure.id'), this.get('msg'));
      } else {
        let sendTo = null;
// TODO send To
        websocket.sendMessage(sendTo, this.get('msg'));
      }
      this.set('msg', '');
    },

    openConversation(id) {
      let websocket = this.get('websocket');

      websocket.set('conversationId', id);
      websocket.getConversationMessages(id);
      websocket.markConversationRead(id);
    }
  }
});
