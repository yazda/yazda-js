import Ember from 'ember';
import moment from 'moment';
import PagingMixin from '../mixins/paging';

export default Ember.Component.extend(PagingMixin, {
  classNames:  ['item-list', 'adventures'],
  store:       Ember.inject.service(),
  geolocation: Ember.inject.service(),

  loading: true,

  userAndClubObserver: Ember.observer('userId', 'clubId', function() {
    this.set('loading', true);
    Ember.run.once(this, 'getModels');
  }),
  adventuresGrouped:   Ember.computed('adventures.[]', function() {
    let adventures = this.get('adventures');
    let currentDate = moment();

    let grouped = adventures.reduce((ret, val) => {
      let date = moment(val.get('start_time'));
      let startTime;

      if(currentDate > date) {
        startTime = currentDate;
      } else {
        startTime = date;
      }

      let adventuresObj = ret.find((value) => {
        return value.key === startTime.format('MMMM Do YYYY');
      });

      if(!adventuresObj) {
        adventuresObj = {
          key:        startTime.format('MMMM Do YYYY'),
          title:      startTime.format('dddd, MMMM Do'),
          adventures: []
        };

        ret.push(adventuresObj);
      }

      if(!adventuresObj.adventures.find(obj => {
            return obj.id === val.id;
          })) {
        adventuresObj.adventures.push(val);
      }

      return ret;
    }, []);

    return grouped;
  }),
  getLocation:         function() {
    let self = this;

    this
        .get('geolocation')
        .getLocation()
        .then(function(geoObject) {
          self.set('loading', true);
          self.set('lat', geoObject.coords.latitude);
          self.set('lon', geoObject.coords.longitude);
        }).catch(() => {
      // TODO display message asking for location and why we need it.
    }).finally(function() {
      Ember.run.once(self, 'getModels');
    });
  }.on('didInsertElement'),
  getModels:           function() {
    let self = this;
    var store = self.get('store');
    var userId = self.get('userId');
    var clubId = self.get('clubId');
    var rsvp;

    if(userId || clubId) {
      rsvp = self.set('adventures',
          store.query('adventure', {
            club_id:   clubId,
            user_id:   userId,
            page:      this.get('page'),
            page_size: this.get('page_size')
          }));
    } else {
      rsvp = self.set('adventures', store.query('adventure', {
        lat:       this.get('lat'),
        lon:       this.get('lon'),
        page:      this.get('page'),
        page_size: this.get('page_size')
      }));
    }

    return rsvp.then((adventures) => {
      self.set('page_count', adventures.get('meta.page_count'));
      self.set('total', adventures.get('meta.total'));
    }).finally(function() {
      self.set('loading', false);
      return arguments;
    });
  }
});
