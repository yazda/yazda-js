import Ember from 'ember';

export default Ember.Component.extend({
  store:   Ember.inject.service(),
  session: Ember.inject.service(),

  canAddFriend: Ember.computed('user.following', 'user.request_sent', function() {
    let following = this.get('user.following');
    let requestSent = this.get('user.request_sent');

    return !following && !requestSent;
  }),
  actions:      {
    openPanel1() {
      this.$('#user-show-tabs').foundation('selectTab', 'panel1');
    },
    openPanel2() {
      this.$('#user-show-tabs').foundation('selectTab', 'panel2');
    },
    addFriend() {
      if(this.get('session.isAuthenticated')) {
        let user = this.get('user');
        let friendship = this.get('store')
            .createRecord('friendship', {user_id: user.id});

        // TODO what do we do if it errors out
        return friendship
            .save()
            .then(function() {
              return user.reload();
            });
      } else {
        // TODO display message why we redirected
        this.router.transitionTo('login');
      }
    }
  }
});
