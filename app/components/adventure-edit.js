import Ember from 'ember';
import moment from 'moment';

export default Ember.Component.extend({
  classNames: ["content-inner-wrapper", " expanded", " row"],

  geolocation: Ember.inject.service(),
  store:       Ember.inject.service(),

  adventure: null,
  lat:       0,
  lon:       0,

  init() {
    this._super(...arguments);

    // TODO set start time properly
    this.set('start_time', moment(this.get('adventure.start_time')).format('YYYY-MM-DD HH:mm'));
    this.set('hours', moment(this.get('adventure.end_time'))
        .diff(moment(this.get('adventure.start_time')), 'hours'));
  },
  changeLat:              Ember.observer('lat', function() {
    this.set('adventure.lat', this.get('lat'));
  }),
  changeLon:              Ember.observer('lon', function() {
    this.set('adventure.lon', this.get('lon'));
  }),
  startTime:              Ember.observer('start_time', function() {
    let startTime = this.get('start_time');

    this.set('adventure.start_time', moment(startTime).toDate());
    this.set('end_time', moment(startTime).add(2, 'hours').format('YYYY-MM-DD HH:mm'));
  }),
  endTime:                Ember.observer('hours', 'adventure.start_time', function() {
    let startTime = this.get('start_time');
    let hours = this.get('hours');
    let endTime = moment(startTime)
        .add(hours, 'hours')
        .format('YYYY-MM-DD HH:mm');

    console.log('setting end time');
    this.set('adventure.end_time', moment(endTime).toDate());
  }),
  getLocation:            function() {
    let self = this;

    this
        .get('geolocation')
        .getLocation()
        .then(function(geoObject) {
          self.set('adventure.lat', geoObject.coords.latitude);
          self.set('adventure.lon', geoObject.coords.longitude);
        });
  }.on('init'),
  actions:                {
    toggleMap() {
      this.$('#googleMap').toggle();
    },
    changeLocation() {
      this.$('#create-location').change();
    },
    selectAdventureType(adventure_type) {
      this.set('adventure.adventure_type', adventure_type);
    },
    save() {
      let self = this;
      let adventure = this.get('adventure');

      return adventure.save()
          .then(function(adventure) {
            self.get('onSave')(adventure.get('id'));
          });
    },
    destroy() {
      let self = this;
      let adventure = this.get('adventure');

      return adventure.destroyRecord()
          .then(function(adventure) {
            self.get('onSave')(adventure.get('id'));
          });
    },
    onLocationChangeHandler(lat, lng) {
      this.set('lat', lat);
      this.set('lon', lng);
    }
  }
});
