import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Component.extend({
  session:        service('session'),
  sessionAccount: service('session-account'),
  websocket:      service('yazda-websockets'),

  actions: {
    login() {
      this.sendAction('onLogin');
    },

    logout() {
      this.get('session').invalidate();
      this.get('websocket').disconnectFromSocket();
    }
  }
});
