import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['button-group', 'social-share'],

  location: document.location.href
});
