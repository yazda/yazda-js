import Ember from 'ember';
import moment from 'moment';

export default Ember.Component.extend({
  classNames: ["content-inner-wrapper", " expanded", " row"],

  geolocation:   Ember.inject.service(),
  store:         Ember.inject.service(),
  flashMessages: Ember.inject.service(),

  adventure: null,
  lat:       0,
  lon:       0,

  init() {
    this._super(...arguments);

    this.set('adventure', this.get('store').createRecord('adventure', {
      privacy: 'public_adventure'
    }));
  },
  changeLat:              Ember.observer('lat', function() {
    this.set('adventure.lat', this.get('lat'));
  }),
  changeLon:              Ember.observer('lon', function() {
    this.set('adventure.lon', this.get('lon'));
  }),
  startTime:              Ember.observer('start_time', function() {
    let startTime = this.get('start_time');

    this.set('adventure.start_time', moment(startTime).toDate());
  }),
  endTime:                Ember.observer('hours', 'adventure.start_time', function() {
    let startTime = this.get('start_time');
    let hours = this.get('hours');
    let endTime = moment(startTime)
        .add(hours, 'hours')
        .format('YYYY-MM-DD HH:mm');

    console.log('setting end time');
    this.set('adventure.end_time', moment(endTime).toDate());
  }),
  mountainBikingSelected: Ember.computed('adventure.adventure_type', function() {
    if(this.get('adventure.adventure_type') === 'mountain_biking') {
      return 'select-active';
    } else {
      return '';
    }
  }),
  bikingSelected:         Ember.computed('adventure.adventure_type', function() {
    if(this.get('adventure.adventure_type') === 'biking') {
      return 'select-active';
    } else {
      return '';
    }
  }),
  hikingSelected:         Ember.computed('adventure.adventure_type', function() {
    if(this.get('adventure.adventure_type') === 'hiking') {
      return 'select-active';
    } else {
      return '';
    }
  }),
  createClubs:            Ember.computed('me.clubs', function() {
    let clubs = this.get('me.clubs').filter(function(club) {
      return club.is_member && club.role !== 'member';
    });

    return clubs;
  }),
  showClubs:              Ember.computed('me.clubs', function() {
    let clubs = this.get('me.clubs').filter(function(club) {
      return club.is_member && club.role !== 'member';
    });

    return clubs && clubs.length > 0;
  }),
  getLocation:            function() {
    let self = this;

    this
        .get('geolocation')
        .getLocation()
        .then(function(geoObject) {
          self.set('adventure.lat', geoObject.coords.latitude);
          self.set('adventure.lon', geoObject.coords.longitude);
        });
  }.on('init'),
  actions:                {
    toggleMap() {
      this.$('#googleMap').toggle();
    },
    changeLocation() {
      this.$('#create-location').change();
    },
    toggleUserInvite(userId) {
      var userIds = this.get('adventure.user_ids');

      if(userIds) {
        var selected;
        const id = userIds.find(function(id) {
          return id === userId;
        });

        if(id) {
          userIds.removeObject(userId);
          selected = false;
        } else {
          userIds.pushObject(userId);
          selected = true;
        }
        return selected;
      } else {
        this.set('adventure.user_ids', [userId]);
        return true;
      }
    },
    selectAdventureType(adventure_type) {
      this.set('adventure.adventure_type', adventure_type);
    },
    save() {
      const flashMessages = this.get('flashMessages');
      let self = this;
      let adventure = this.get('adventure');

      return adventure.save()
          .then(function(adventure) {
            self.get('onSave')(adventure.get('id'));
          }).catch(function() {
            self.$('#adventure-create-tabs').foundation('selectTab', 'panel1');
            flashMessages.alert('There was an error saving your adventure.');
          });
    },
    openPanel2() {
      this.$('#adventure-create-tabs').foundation('selectTab', 'panel2');
    },
    onLocationChangeHandler(lat, lng) {
      this.set('lat', lat);
      this.set('lon', lng);
    }
  }
});
