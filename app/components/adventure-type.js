import Ember from 'ember';

export default Ember.Component.extend({
  type: Ember.computed('adventureType', function() {
    let type= this.get('adventureType').replace('_',' ');

    return type.capitalize();
  }),
  typeIcon: Ember.computed('adventureType', function() {
    let type= this.get('adventureType');

    switch(type) {
      case 'hiking':
        return 'icon-hiking';
      case 'biking':
        return 'icon-biking';
      case 'mountain_biking':
        return 'icon-mountain-biking';
      case 'running':
        return 'icon-running';
      case 'trail_running':
        return 'icon-trail-running';
      default:
        return '';
    }
  })
});
