import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['show-user-image'],
  profileImageUrl: Ember.computed('user.profile_image_url', function() {
    return this.get('user.profile_image_url');
  })
});
