import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Component.extend({
  session: service('session'),
  ajax:    service(),
  store:   service(),
  metrics: service(),

  init() {
    this._super(...arguments);
    this.set('user', this.get('store').createRecord('register', {}));
  },
  actions: {
    register() {
      let self = this;
      let user = this.get('user');

      user.set('password_confirmation', user.get('password'));
      user.set('newsletter', true);

      return user.save()
          .then(() => {
            return self
                .get('session')
                .authenticate('authenticator:oauth2', user.get('email'), user.get('password'))
                .then(function() {
                  self.get('metrics').trackEvent({'event': 'Lead'});
                  self.get('metrics').trackEvent({'event': 'CompleteRegistration'});

                  if(typeof Abba !== 'undefined') {
                    Abba('Registration Bullets - 1').complete();
                  }
                  return arguments;
                });
          }).catch(() => {
            self.set('errorMessage', "Oops you've got some errors. Try again.");
          });
    },

    authenticateWithFacebook() {
      let self = this;

      return this.get('session').authenticate('authenticator:torii', 'facebook').then(function() {
        self.get('metrics').trackEvent({'event': 'Lead'});
        self.get('metrics').trackEvent({'event': 'CompleteRegistration'});

        if(typeof Abba !== 'undefined') {
          Abba('Registration Bullets - 1').complete();
        }
        return arguments;
      });
    },
    authenticateWithStrava() {
      let self = this;

      return this.get('session').authenticate('authenticator:torii', 'strava').then(function() {
        self.get('metrics').trackEvent({'event': 'Lead'});
        self.get('metrics').trackEvent({'event': 'Registration'});

        if(typeof Abba !== 'undefined') {
          Abba('Registration Bullets - 1').complete();
        }
        return arguments;
      });
    }
  }
});
