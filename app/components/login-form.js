import Ember from 'ember';
import RedirectMixin from '../mixins/redirect';

const {service} = Ember.inject;

export default Ember.Component.extend(RedirectMixin, {
  session: service('session'),

  actions: {
    authenticateWithOAuth2() {
      let {identification, password} = this.getProperties('identification', 'password');
      var self = this;

      return this
          .get('session')
          .authenticate('authenticator:oauth2', identification, password)
          .catch(() => {
            self.set('errorMessage', "Your email and password don't seem to be" +
                " correct. Try again.");
          });
    },

    authenticateWithFacebook() {
      return this.get('session').authenticate('authenticator:torii', 'facebook');
    },
    authenticateWithStrava() {
      return this.get('session').authenticate('authenticator:torii', 'strava');
    }
  }
});
