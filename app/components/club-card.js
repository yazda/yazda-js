import Ember from 'ember';

export default Ember.Component.extend({
  session:       Ember.inject.service(),
  flashMessages: Ember.inject.service(),

  actions: {
    joinClub() {
      const flashMessages = this.get('flashMessages');

      if(this.get('session.isAuthenticated')) {
        var club = this.get('club');
        var self = this;

        return club
            .join()
            .then(function() {
              club.set('is_member', true);
              flashMessages.success("You've joined " + self.get('club.name'));
            }).catch(() => {
              flashMessages.alert('We encountered an error. Please try again.');
            });
      } else {
        this.router.transitionTo('login');
      }
    }
  }
});
