import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Component.extend({
  store:   service('store'),
  users:   [],
  loading: true,

  usersLimited: Ember.computed('users.[]', function() {
    return this.get('users').slice(0, 3);
  }),

  show: Ember.computed('loading', 'users.[]', function() {
    return !this.get('loading') && this.get('users.length') > 0;
  }),

  getPotentialFriends: function() {
    var store = this.get('store');
    var self = this;
    var users = this.get('users');

    store.query('user', {endpoint: 'find_strava_users'})
      .then((data) => {
        users.pushObjects(data.toArray());
      }).finally(() => {
      self.set('loading', false);
    });
    store.query('user', {endpoint: 'find_facebook_users'})
      .then((data) => {
        users.pushObjects(data.toArray());
      }).finally(() => {
      self.set('loading', false);
    });
    store.query('user', {endpoint: 'search'})
      .then((data) => {
        users.pushObjects(data.toArray());
      }).finally(() => {
      self.set('loading', false);
    });
  }.on('didInsertElement'),

  actions: {
    addFriend(userId) {
      var self = this;
      var friendship = this.get('store')
        .createRecord('friendship', {user_id: userId});

      // TODO what do we do if it errors out
      friendship
        .save()
        .then(function() {
          var record = self.get('users').findBy('id', userId);
          self.get('users').removeObject(record);
        });
    }
  }
});
