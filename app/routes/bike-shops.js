import Ember from 'ember';
import MetaTagMixin from '../mixins/meta-tag';

export default Ember.Route.extend(MetaTagMixin, {
  titleToken() {
    return 'Yazda For Bike Shops';
  },

  _createHeadTags() {
    let description = 'Create and share bike shop rides and demo days with' +
        ' your existing customers while advertising at zero cost and' +
        ' attracting new customers.';

    let tags = [
      this.get('metaTags')
          .createTag('og:type', 'website'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:image', 'https://yazdaapp.com/img/backgrounds/image10.jpg'),
      this.get('metaTags')
          .createTag('og:title', 'Yazda For Bike Shops'),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', 'Yazda For Bike Shops'),
      this.get('metaTags')
          .createTag('twitter:image', 'https://yazdaapp.com/img/backgrounds/image10.jpg'),
      this.get('metaTags')
          .createTag('description', description),
      this.get('metaTags')
          .createTag('twitter:description', description)];

    return tags;
  }
});
