import Ember from 'ember';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

export default Ember.Route.extend(UnauthenticatedRouteMixin, {
  titleToken: 'Reset Password',

  _createHeadTags() {
    let description = 'You may have jumped off your bike, but you didn\'t' +
        ' fall! We\'ve got your back on hitting the reset button when it' +
        ' comes to your Yazda account.';

    let tags = [
      this.get('metaTags')
          .createTag('og:type', 'website'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:title', 'Yazda - Reset Password'),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', 'Yazda - Reset Password'),
      this.get('metaTags')
          .createTag('description', description),
      this.get('metaTags')
          .createTag('twitter:description', description)];

    return tags;
  }
});
