import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';
import RedirectMixin from '../mixins/redirect';
import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Route.extend(RedirectMixin, UnauthenticatedRouteMixin, {
  session: service('session'),

  titleToken() {
    return 'Login';
  },

  beforeModel() {
    let resp = this.redirect();

    if(resp) {
      return resp;
    } else {
      return this._super(...arguments);
    }
  },

  _createHeadTags() {
    let description = 'To view, create or share group bike rides, log in to' +
        ' your Yazda account and let the trail fun and adventures begin.';

    let tags = [
      this.get('metaTags')
          .createTag('og:type', 'website'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:title', 'Login - Yazda'),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', 'Login - Yazda'),
      this.get('metaTags')
          .createTag('description', description),
      this.get('metaTags')
          .createTag('twitter:description', description)];

    return tags;
  },

  actions: {
    didTransition() {
      Ember.run.scheduleOnce('afterRender', this, () => {
        this.transitionTo('login', {queryParams: {r: null}});
      });
    },

    willTransition(transition) {
      if(transition.intent.name === 'index' && (this.getRedirect() && this.getRedirect() !== '')) {
        transition.abort();
      } else {
        return true;
      }
    }
  }
});
