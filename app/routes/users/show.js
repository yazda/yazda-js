import Ember from 'ember';
import MetaTagMixin from '../../mixins/meta-tag';

export default Ember.Route.extend(MetaTagMixin, {
  titleToken(model) {
    return model.user.get('name');
  },

  model(params) {
    return Ember.RSVP.hash({
      user: this.store.findRecord('user', params.id)
    });
  },

  setupController(controller, model) {
    controller.set('user', model.user);
  },

  _createHeadTags(model) {
    var tags = [
      this.get('metaTags')
        .createTag('og:type', 'profile'),
      this.get('metaTags')
        .createTag('og:url', document.location.href),
      this.get('metaTags')
        .createTag('og:image', model.user.get('profile_image_url')),
      this.get('metaTags')
        .createTag('og:title', model.user.get('name') + ' - Yazda'),
      this.get('metaTags')
        .createTag('twitter:card', 'summary'),
      this.get('metaTags')
        .createTag('twitter:title', model.user.get('name') + ' - Yazda'),
      this.get('metaTags')
        .createTag('twitter:image', model.user.get('profile_image_url'))
    ];

    if(model.user.get('description')) {
      tags.push(this.get('metaTags')
        .createTag('description', model.user.get('description')));
      tags.push(this.get('metaTags')
        .createTag('twitter:description', model.user.get('description')));
    }

    return tags;
  }
});
