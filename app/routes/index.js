import Ember from 'ember';
import InfinityRoute from 'ember-infinity/mixins/route';

export default Ember.Route.extend(InfinityRoute, {
  session: Ember.inject.service(),

  perPageParam:    'page_size',     // instead of 'per_page'
  pageParam:       'page',          // instead of 'page'
  totalPagesParam: 'meta.page_count',    // instead of 'meta.total_pages'

  model() {
    let self = this;

    if(this.get('session.isAuthenticated')) {
      return Ember.RSVP.hash({
        me: this.store.findRecord('account', 'me'),
        adventures: self.infinityModel('adventure', {
          perPage:      10,
          startingPage: 1,
          modelPath: 'controller.model.adventures'
        })
      });
    }
  },

  // setupController(controller, model) {
  //   if(this.get('session.isAuthenticated')) {
  //     controller.set('me', model.me);
  //     controller.set('adventures', model.adventures);
  //   }
  // },

  actions: {
    didTransition() {
      if(!this.get('session.isAuthenticated')) {

        Ember.run.scheduleOnce('afterRender', this, () => {
          if(typeof Abba !== 'undefined') {
            Abba('Registration Bullets - 1')
                .control()
                .variant('Bullets: Enabled', function() {
                  let form = $('.homepage-register-form').parent();

                  $('.ab_1_registration').toggle();
                  $('.ab_1_registration').children('.columns:last').append(form);
                  $('#userName').parent().removeClass('large-3').addClass('large-12');
                  $('#password').parent().removeClass('large-3').addClass('large-6');
                  $('#emailAddress').parent().removeClass('large-3').addClass('large-6');
                  $('input[type="submit"]').closest('.large-3').removeClass('large-3').addClass('large-12');
                })
                .start();
          }
        });
      }
    }
  },

  _createHeadTags() {
    let description = 'Use Yazda to schedule and share your group bike rides.';

    let tags = [
      this.get('metaTags')
          .createTag('og:type', 'website'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:image', 'https://yazdaapp.com/img/logo/yazda_full_horizontal_shadow.png'),
      this.get('metaTags')
          .createTag('og:title', 'Yazda - Schedule and Share Group Bike Rides'),
      this.get('metaTags')
          .createTag('twitter:card', 'app'),
      this.get('metaTags')
          .createTag('twitter:title', 'Yazda - Schedule and Share Group Bike Rides'),
      this.get('metaTags')
          .createTag('twitter:image', 'https://yazdaapp.com/img/logo/yazda_full_horizontal_shadow.png'),
      this.get('metaTags')
          .createTag('description', description),
      this.get('metaTags')
          .createTag('twitter:description', description),
      this.get('metaTags')
          .createTag('twitter:app:country', 'US'),
      this.get('metaTags')
          .createTag('twitter:app:name:iphone', 'Yazda'),
      this.get('metaTags')
          .createTag('twitter:app:name:googleplay', 'Yazda'),
      this.get('metaTags')
          .createTag('twitter:app:id:iphone', '983846853'),
      this.get('metaTags')
          .createTag('twitter:app:id:googleplay', 'com.yazda.yazda_ios'),
      this.get('metaTags')
          .createTag('twitter:app:url:iphone', 'https://itunes.apple.com/us/app/yazda/id983846853?ls=1&mt=8'),
      this.get('metaTags')
          .createTag('twitter:app:url:googleplay', 'https://play.google.com/store/apps/details?id=com.yazda.yazda_ios')];

    return tags;
  },

  meta: {
    twitter: {
      card:        'app',
      description: 'Yazda is a mobile app for mountain bikers and hikers to' +
                   ' setup group adventures.',
      app:         {
        country: 'US',
        name:    {
          iphone:     'Yazda',
          googleplay: 'Yazda'
        },
        id:      {
          iphone:     '983846853',
          googleplay: 'com.yazda.yazda_ios'
        },
        url:     {
          iphone:     'https://itunes.apple.com/us/app/yazda/id983846853?ls=1&mt=8',
          googleplay: 'https://play.google.com/store/apps/details?id=com.yazda.yazda_ios'
        }
      }
    }
  }
});
