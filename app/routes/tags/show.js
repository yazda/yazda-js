import Ember from 'ember';

export default Ember.Route.extend({
  session: Ember.inject.service(),

  model(params) {
    return this.store.findRecord('tag', params.id);
  },

  titleToken(model) {
    return '#' + model.get('name');
  },

  _createHeadTags(model) {
    let description = 'Find clubs or adventures on Yazda with popular' +
        ' hashtags like #mtblife';

    let tags = [
      this.get('metaTags')
          .createTag('og:type', 'website'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:title', model.get('id') + ' - Yazda'),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', model.get('id') + ' - Yazda'),
      this.get('metaTags')
          .createTag('description', description),
      this.get('metaTags')
          .createTag('twitter:description', description)];

    return tags;
  }
});
