import Ember from 'ember';
import InfinityRoute from 'ember-infinity/mixins/route';

export default Ember.Route.extend(InfinityRoute, {
  titleToken:  'Tags',
  queryParams: {
    q: {
      refreshModel: true,
      replace:      true
    }
  },

  perPageParam:    'page_size',     // instead of 'per_page'
  pageParam:       'page',          // instead of 'page'
  totalPagesParam: 'meta.page_count',    // instead of 'meta.total_pages'

  model(params) {
    delete params.page;
    delete params.page_size;

    return this.infinityModel('tag',
        $.extend({}, params, {
          perPage:      10,
          startingPage: 1
        })
    );
  },

  _createHeadTags() {
    let description = 'Find, create, and share biking adventures and bike' +
        ' clubs with popular hashtags like #mtblife';

    let tags = [
      this.get('metaTags')
          .createTag('og:type', 'website'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:title', 'Tags - Yazda'),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', 'Tags - Yazda'),
      this.get('metaTags')
          .createTag('description', description),
      this.get('metaTags')
          .createTag('twitter:description', description)];

    return tags;
  }
});
