import Ember from 'ember';
import RedirectMixin from '../mixins/redirect';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import ResetScrollMixin from 'ember-cli-reset-scroll';

const {service} = Ember.inject;

// TODO redirect to login on 401 error
export default Ember.Route.extend(RedirectMixin, ApplicationRouteMixin, ResetScrollMixin, {
  titleToken:     'Yazda',
  title(tokens) {
    tokens = Ember.makeArray(tokens);
    return tokens.reverse().join(' - ');
  },
  sessionAccount: service('session-account'),

  beforeModel() {
    return this._loadCurrentUser();
  },

  sessionAuthenticated() {
    let resp = this.redirect();

    if(!resp) {
      this._super(...arguments);
      this._loadCurrentUser().catch(() => this.get('session').invalidate());
    }
  },

  _loadCurrentUser() {
    return this.get('sessionAccount').loadCurrentUser();
  },

  actions: {
    error(error) {
      if(error && error.errors && error.errors[0].status === '401') {
        return this.transitionTo('login');
      }
    }
  }
});
