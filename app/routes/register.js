import Ember from 'ember';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

export default Ember.Route.extend(UnauthenticatedRouteMixin, {
  titleToken() {
    return 'Register';
  },

  _createHeadTags() {
    let description = 'Register with Yazda for a quick, easy, and FREE way' +
        ' to spend more time with your friends on bikes. It\'s the best way' +
        ' to schedule, share, and find group rides.';

    let tags = [
      this.get('metaTags')
          .createTag('og:type', 'website'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:title', 'Register - Yazda'),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', 'Register - Yazda'),
      this.get('metaTags')
          .createTag('description', description),
      this.get('metaTags')
          .createTag('twitter:description', description)];

    return tags;
  }
});
