import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  titleToken(model) {
    return 'Edit ' + model.club.get('name');
  },

  model(params) {
    return Ember.RSVP.hash({
      club: this.store.findRecord('club', params.id),
      me:   this.store.findRecord('account', 'me')
    });
  },
  setupController(controller, model) {
    controller.set('me', model.me);
    controller.set('club', model.club);
  },
  afterModel(model) {
    let redirect = true;
    let clubs = model.me.get('clubs');

    clubs.forEach(function(club) {
      if(Number(club.id) === Number(model.club.id) && (club.role === 'admin' || club.role === 'owner')) {
        redirect = false;
      }
    });

    if(redirect) {
      // TODO show error saying no access
      this.transitionTo('index');
    }
  }
});
