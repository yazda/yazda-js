import Ember from 'ember';
import MetaTagMixin from '../../mixins/meta-tag';

export default Ember.Route.extend(MetaTagMixin, {
  session: Ember.inject.service(),

  model(params, transition) {
    if(!this.get('session.isAuthenticated')) {
      this.set('session.attemptedTransition', transition);
    }

    return Ember.RSVP.hash({
      club: this.store.findRecord('club', params.id)
    });
  },

  setupController(controller, model) {
    controller.set('club', model.club);
  },

  titleToken(model) {
    return model.club.get('name');
  },

  _createHeadTags(model) {
    var tags = [
      this.get('metaTags')
        .createTag('og:type', 'profile'),
      this.get('metaTags')
        .createTag('og:url', document.location.href),
      this.get('metaTags')
        .createTag('og:image', model.club.get('profile_image_url')),
      this.get('metaTags')
        .createTag('og:title', model.club.get('name') + ' - Yazda'),
      this.get('metaTags')
        .createTag('twitter:card', 'summary'),
      this.get('metaTags')
        .createTag('twitter:title', model.club.get('name') + ' - Yazda'),
      this.get('metaTags')
        .createTag('twitter:image', model.club.get('profile_image_url'))
    ];

    if(model.club.get('description')) {
      tags.push(this.get('metaTags')
        .createTag('twitter:description', model.club.get('description')));
      tags.push(this.get('metaTags')
        .createTag('description', model.club.get('description')));
    }

    return tags;
  }
});
