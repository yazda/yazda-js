import Ember from 'ember';
import InfinityRoute from 'ember-infinity/mixins/route';

export default Ember.Route.extend(InfinityRoute, {
  queryParams: {
    q:   {
      refreshModel: true,
      replace:      true
    },
    tag: {
      refreshModel: true,
      replace:      true
    }
  },

  perPageParam:    'page_size',     // instead of 'per_page'
  pageParam:       'page',          // instead of 'page'
  totalPagesParam: 'meta.page_count',    // instead of 'meta.total_pages'

  titleToken() {
    return 'Clubs';
  },

  model(params) {
    delete params.page;
    delete params.page_size;

    return this.infinityModel('club',
        $.extend({}, params, {
          perPage:      10,
          startingPage: 1
        })
    );
  },

  setupController(controller) {
    this._super(...arguments);

    let tag = controller.get('tag');
    let q = controller.get('q');

    if(tag.length) {
      controller.set('search', '#' + tag);
    } else {
      controller.set('search', q);
    }
  },

  _createHeadTags() {
    let description = 'Check out all the happenings at your favorite local' +
        ' bike clubs. If you don\'t see your bike club listed, contact them' +
        ' about partnering with Yazda!';

    let tags = [
      this.get('metaTags')
          .createTag('og:type', 'website'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:title', 'Clubs - Yazda'),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', 'Clubs - Yazda'),
      this.get('metaTags')
          .createTag('description', description),
      this.get('metaTags')
          .createTag('twitter:description', description)];

    return tags;
  }
});
