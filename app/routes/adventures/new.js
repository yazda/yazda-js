import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  titleToken() {
    return 'New Adventure';
  },

  model() {
    return Ember.RSVP.hash({
      me: this.store.findRecord('account', 'me')
    });
  },

  setupController(controller, model) {
    controller.set('me', model.me);
  }
});
