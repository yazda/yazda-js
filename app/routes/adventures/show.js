import Ember from 'ember';
import MetaTagMixin from '../../mixins/meta-tag';

export default Ember.Route.extend(MetaTagMixin, {
  session: Ember.inject.service(),

  titleToken(model) {
    return model.get('name');
  },

  model(params, transition) {
    if(!this.get('session.isAuthenticated')) {
      // TODO isn't transitioning properly for some reason
      this.set('session.attemptedTransition', transition);
    }

    return this.store.findRecord('adventure', params.id);
  },

  _createHeadTags(model) {
    var tags = [
      this.get('metaTags')
          .createTag('og:type', 'place'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:title', model.get('name') + ' - Yazda'),
      this.get('metaTags')
          .createTag('place:location:latitude', model.get('lat')),
      this.get('metaTags')
          .createTag('place:location:longitude', model.get('lon')),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', model.get('name') + ' - Yazda')
    ];

    if(model.get('description')) {
      tags.push(this.get('metaTags')
          .createTag('twitter:description', model.get('description')));

      tags.push(this.get('metaTags')
          .createTag('description', model.get('description')));
    }

    if(model.get('club.banner_image_url')) {
      tags.push(this.get('metaTags')
          .createTag('twitter:image', model.get('club.banner_image_url')));
      tags.push(this.get('metaTags')
          .createTag('og:image', model.get('club.banner_image_url')));
    }

    return tags;
  },

  actions: {
    didTransition() {
      Ember.run.scheduleOnce('afterRender', this, () => {
        let buttons = $('.ya-na-btn');

        if(typeof Abba !== 'undefined') {
          Abba('Ya Na Buttons - 2')
              .control()
              .variant('Buttons - White', function() {
                buttons.addClass('ab-white-ya-na');
              })
              .start();
        }
      });
    }
  }
});
