import Ember from 'ember';
import InfinityRoute from 'ember-infinity/mixins/route';

export default Ember.Route.extend(InfinityRoute, {
  titleToken:  'Adventures',
  queryParams: {
    tag: {
      refreshModel: true,
      replace:      true
    }
  },

  perPageParam:    'page_size',     // instead of 'per_page'
  pageParam:       'page',          // instead of 'page'
  totalPagesParam: 'meta.page_count',    // instead of 'meta.total_pages'

  model(params) {
    delete params.page;
    delete params.page_size;

    return this.infinityModel('adventure',
        $.extend({}, params, {
          perPage:      10,
          startingPage: 1
        })
    );
  },

  setupController(controller) {
    this._super(...arguments);

    controller.set('search', controller.get('tag'));
  },

  _createHeadTags() {
    let description = 'Check out your friends\' group bike rides and biking' +
        ' events in your area. If there\'s something you don\'t see, add it!';

    var tags = [
      this.get('metaTags')
          .createTag('og:type', 'website'),
      this.get('metaTags')
          .createTag('og:url', document.location.href),
      this.get('metaTags')
          .createTag('og:title', 'Adventures - Yazda'),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', 'Adventures - Yazda'),
      this.get('metaTags')
          .createTag('description', description),
      this.get('metaTags')
          .createTag('twitter:description', description)
    ];

    return tags;
  }
});
