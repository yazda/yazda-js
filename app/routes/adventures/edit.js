import Ember from 'ember';
import MetaTagMixin from '../../mixins/meta-tag';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, MetaTagMixin, {
  session: Ember.inject.service(),

  titleToken(model) {
    return model.get('name');
  },

  model(params, transition) {
    if(!this.get('session.isAuthenticated')) {
      this.set('session.attemptedTransition', transition);
    }

    return this.store.findRecord('adventure', params.id);
  },

  afterModel(model) {
    if(!model.get('can_edit')) {
      this.router.transitionTo('adventures.show', model.get('id'));
    }
  },

  _createHeadTags(model) {
    var tags = [
      this.get('metaTags')
          .createTag('og:type', 'place'),
      this.get('metaTags')
          .createTag('og:url', document.location),
      this.get('metaTags')
          .createTag('og:title', model.get('name')),
      this.get('metaTags')
          .createTag('place:location:latitude', model.get('lat')),
      this.get('metaTags')
          .createTag('place:location:longitude', model.get('lon')),
      this.get('metaTags')
          .createTag('twitter:card', 'summary'),
      this.get('metaTags')
          .createTag('twitter:title', model.get('name'))
    ];

    if(model.get('description')) {
      tags.push(this.get('metaTags')
          .createTag('twitter:description', model.get('description')));

      tags.push(this.get('metaTags')
          .createTag('description', model.get('description')));
    }

    if(model.get('club.banner_image_url')) {
      tags.push(this.get('metaTags')
          .createTag('twitter:image', model.get('club.banner_image_url')));
      tags.push(this.get('metaTags')
          .createTag('og:image', model.get('club.banner_image_url')));
    }

    return tags;
  }
});
