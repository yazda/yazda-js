import Ember from 'ember';
import InfinityRoute from 'ember-infinity/mixins/route';

export default Ember.Route.extend(InfinityRoute, {
  titleToken() {
    return 'Rejected - ';
  },

  perPageParam:    'page_size',     // instead of 'per_page'
  pageParam:       'page',          // instead of 'page'
  totalPagesParam: 'meta.page_count',    // instead of 'meta.total_pages'

  model(params) {
    delete params.page;
    delete params.page_size;

    return this.infinityModel('invite',
        $.extend({}, params, {
          endpoint:     'rejected',
          adventure_id: this.modelFor('adventures/show').get('id'),
          perPage:      20,
          startingPage: 1
        })
    );
  }
});
