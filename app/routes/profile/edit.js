import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  titleToken() {
    return 'Edit Profile';
  },

  model() {
    return Ember.RSVP.hash({
      user: this.store.findRecord('account', 'me')
    });
  },

  setupController(controller, model) {
    controller.set('user', model.user);
  }
});
