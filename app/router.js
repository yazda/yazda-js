import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  metrics:  Ember.inject.service(),

  didTransition() {
    this._super(...arguments);

    Ember.run.scheduleOnce('afterRender', this, () => {
      const page = this.get('url');
      const title = this.getWithDefault('currentRouteName', 'unknown');

      Ember.get(this, 'metrics').trackPage({page, title});

      branch.banner({
        icon:                  'https://yazdaapp.com/img/icons/apple-icon-180x180.png',
        title:                 'Yazda',
        description:           'Schedule group bike rides.',
        downloadAppButtonText: 'FREE',
        showDesktop:           false,
        mobileSticky:          true,
        forgetHide:            3,
        make_new_link:         true,
        theme:                 'dark',
        customCSS:             '.icon img {border-radius:15px;}'
      }, {
        data: {
          'url': 'yazda://web' + page
        }
      });
    });
  },
});

Router.map(function() {
  this.route('bike-riders');
  this.route('bike-shops');
  this.route('bike-clubs');

  this.route('login', {path: '/sign_in'});
  this.route('reset-password', {path: '/reset_password'});
  this.route('register');

  this.route('tags', function() {
    this.route('show', {path: '/:id'});
  });

  this.route('adventures', function() {
    this.route('new');
    this.route('edit', {path: '/:id/edit'});
    this.route('show', {path: '/:id'}, function() {
      this.route('attendings');
      this.route('pendings');
      this.route('rejecteds');
    });
  });

  this.route('users', function() {
    this.route('show', {path: '/:id'});
  });
  this.route('clubs', function() {
    this.route('new');
    this.route('show', {path: '/:id'});
    this.route('edit', {path: '/:id/edit'});
  });

  this.route('profile', function() {
    this.route('edit');
  });
});

export default Router;
