import Ember from 'ember';
import _ from 'underscore';
import config from '../config/environment';
import moment from 'moment';

const {service} = Ember.inject;

export default Ember.Service.extend({
  socketIo:       service('socket-io'),
  session:        service('session'),
  sessionAccount: service('session-account'),

  socketRef:          null,
  conversationId:     null,
  _isConnected:       false,
  _messages:          [],
  _adventureMessages: [],
  _channels:          [],

  init() {
    this._super(...arguments);

    const socket = this.get('socketIo').socketFor(config.socket, {
      reconnection:         true,
      reconnectionDelay:    1000,
      reconnectionDelayMax: 10000,
      reconnectionAttempts: 5,
      transports:           ['websocket', 'polling']
    });

    this.set('socketRef', socket);

    socket.on('connect', this._authenticate, this);
    socket.on('connecting', this._connecting, this);
    socket.on('disconnect', this._disconnect, this);
    socket.on('connect_failed', this._connect_failed, this);
    socket.on('error', this._error, this);
    socket.on('reconnect_failed', this._reconnect_failed, this);
    socket.on('reconnect', this._reconnect, this);
    socket.on('reconnecting', this._reconnecting, this);
    socket.on('authenticated', this._authenticated, this);

    //Conversations
    socket.on('direct_chat', this._directChat, this);
    socket.on('conversations', this._conversationsStream, this);
    socket.on('conversations:add', this._conversationsAdd, this);
  },

  disconnectFromSocket() {
    this._super(...arguments);

    const socket = this.get('socketRef');

    if(socket) {
      socket.off('connect', this._authenticate);
      socket.off('connecting', this._connecting);
      socket.off('disconnect', this._disconnect);
      socket.off('connect_failed', this._connect_failed);
      socket.off('error', this._error);
      socket.off('reconnect_failed', this._reconnect_failed);
      socket.off('reconnect', this._reconnect);
      socket.off('reconnecting', this._reconnecting);

      //Conversations
      socket.off('direct_chat', this._directChat);
      socket.off('conversations', this._conversationsStream);
      socket.off('conversations:add', this._conversationsAdd);

      socket.closeSocketFor(config.socket);
    }
  },

  // Adventures

  sendMessageToAdventurers(adventureId, message) {
    let socket = this.get('socketRef');
    let obj = {
      adventureId: adventureId,
      msg:         message
    };

    socket.emit('chat', obj);
  },

  getAdventureMessages(id) {
    let storedId = this.get('adventureId');

    this.removeAdventureMessages(storedId);

    this.set('adventureId', id);

    let socket = this.get('socketRef');
    let obj = {adventureId: id};

    socket.on(`adventures:${id}`, this._receiveAdventureMessage, this);
    socket.on(`adventures:${id}:paged`, this._receiveAdventureMessages, this);

    if(!this.get('_isConnected')) {
      this.get('_channels').pushObject(obj);
    } else {
      socket.emit('join', obj);
    }
  },

  removeAdventureMessages(id) {
    let storedId = this.get('adventureId');

    if(storedId) {
      this.set('adventureId', null);

      let socket = this.get('socketRef');
      let adventureMessages = this.get('_adventureMessages');

      socket.emit('leave', {adventureId: id});
      socket.off(`adventures:${id}`, this._receiveAdventureMessage);
      socket.off(`adventures:${id}:paged`, this._receiveAdventureMessages);

      adventureMessages.clear();
    }
  },

  markAdventuresRead(id) {
    let socket = this.get('socketRef');
    let adventureMessages = this.get('_adventureMessages');
    let meId = Number.parseFloat(this.get('sessionAccount.account.id'));
    let obj = {
      adventureId: id,
      action:      'mark_read'
    };

    socket.emit('conversations', obj);

    adventureMessages.forEach((msg) => {
      if(!msg.read) {
        msg.read = [];
      }

      msg.read.push(meId);
    });
    adventureMessages.notifyPropertyChange('[]');
  },

  _receiveAdventureMessage(msg) {
    let adventureMessages = this.get('_adventureMessages');

    adventureMessages.addObject(msg);
  },

  _receiveAdventureMessages(msgs) {
    let adventureMessages = this.get('_adventureMessages');

    adventureMessages.addObjects(msgs);
  },

  adventureMessages: function() {
    return this.get('_adventureMessages');
  }.property('_adventureMessages.[]'),

  adventureUnread: function() {
    let messages = this.get('_adventureMessages');
    let id = Number.parseFloat(this.get('sessionAccount.account.id'));
    let unread = _.filter(messages, function(obj) {
      return !_.contains(obj.read, id);
    });

    console.log('new unread');

    return unread.length;
  }.property('_adventureMessages.[]'),

  _directChat(chat) {
    this.get('_messages').pushObject(chat);
  },

  unread: function() {
    let messages = this.get('_messages');

    return _.reduce(messages, function(num, obj) {
      return num + obj.unread;
    }, 0);
  }.property('_messages.[]'),

  conversations: function() {
    let messages = _.uniq(Array.from(this.get('_messages')).reverse(),
        true,
        (a) => a.conversationId);

    return _.sortBy(messages, function(message) {
      return moment(message.sent_at);
    }).reverse();
  }.property('_messages.[]'),

  messages: function() {
    let conversationId = this.get('conversationId');

    return _.where(this.get('_messages'), {conversationId: conversationId});
  }.property('_messages.[]', 'conversationId'),

  sendMessage(sendTo, message) {
    let conversationId = this.get('conversationId');
    let socket = this.get('socketRef');

    socket.emit('chat', {
      msg:            message,
      sendTo:         sendTo,
      conversationId: conversationId
    });
  },

  markConversationRead(conversationId) {
    let socket = this.get('socketRef');
    let messages = this.get('_messages');
    let obj = {
      conversationId: conversationId,
      action:         'mark_read'
    };

    messages
        .filter((message) => message.conversationId === conversationId)
        .setEach('unread', 0);

    socket.emit('conversations', obj);
    this.get('_messages').notifyPropertyChange('[]');
  },

  getConversationMessages(conversationId) {
    let socket = this.get('socketRef');
    let obj = {
      conversationId: conversationId,
      action:         'list'
    };

    socket.emit('conversations', obj);
  },

  _conversationsAdd(convMessages) {
    let messages = this.get('_messages');

    convMessages.forEach((message) => {
      let foundMessage = _.filter(messages, (m) => {
            return m.id === message._id || m._id === message._id;
          }).length > 0;

      if(!foundMessage) {
        if(!message.unread) {
          message.unread = 0;
        }

        messages.push(message);
      }
    });

    messages = _.sortBy(messages, (m) => moment(m.sent_at).unix());

    this.set('_messages', messages);
    this.get('_messages').notifyPropertyChange('[]');
  },

  _conversationsStream(conversations) {
    let messages = this.get('_messages');

    messages = conversations.messages;

    this.set('_messages', messages);
  },

  _connecting() {
    console.log('connecting');
  },

  _disconnect() {
    console.log('disconnect');
    this.set('_isConnected', false);
  },

  _connect_failed() {
    console.log('connect_failed');
  },

  _error(err) {
    console.log('error: ' + err);
  },

  _reconnect_failed() {
    console.log('reconnect_failed');
  },

  _reconnect() {
    console.log('reconnected');
  },

  _reconnecting() {
    console.log('reconnecting');
  },

  _authenticate() {
    let token = this.get('session.data.authenticated');
    let socket = this.get('socketRef');

    socket.emit('authentication', {token: token.access_token});
  },

  _authenticated() {
    let channels = this.get('_channels');
    let socket = this.get('socketRef');
    this.set('_isConnected', true);

    channels.forEach((obj) => {
      socket.emit('join', obj);
    });
  }
});
