import Ember from 'ember';

export default Ember.Service.extend({
  createTag(id, content) {
    if(id && content) {
      return {type: 'meta', tagId: id, attrs: {property: id, content: content}};
    }
  }
});
