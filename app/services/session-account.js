import Ember from 'ember';

const {inject: {service}, RSVP} = Ember;

export default Ember.Service.extend({
  session: service('session'),
  store:   service(),

  loadCurrentUser() {
    return new RSVP.Promise((resolve, reject) => {
      const account = this.get('session.data.authenticated');

      if(!account.access_token) {
        resolve();
      } else {
        return this.get('store').find('account', 'me').then((account) => {
          // appboy.changeUser(account.id);
          this.set('account', account);
          resolve();
        }, reject);
      }
    });
  }
});
