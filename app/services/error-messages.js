import Ember from 'ember';

export default Ember.Service.extend({
  errors: [],
  addError(error) {
    this.get('errors').pushObject(error);
  },
  clearErrors() {
    this.get('errors').clear();
  },
  getErrors() {
    return this.get('errors');
  }
});
