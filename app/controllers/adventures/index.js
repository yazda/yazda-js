import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['page', 'page_size', 'tag'],

  page: 1,
  page_size: 10,
  tag: '',

  watchSearch: function() {
    Ember.run.debounce(this, this.runSearch, 750);
  }.observes("search"),

  runSearch: function(){
    this.set('tag', this.get('search'));
  }
});
