import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    save(adventureId) {
      this.transitionToRoute('adventures.show', adventureId);
    }
  }
});
