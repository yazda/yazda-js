import Ember from 'ember';
import moment from 'moment';
import AdventureMixin from 'yazda-js/mixins/adventure';
import config from '../../config/environment';

export default Ember.Controller.extend(AdventureMixin, {
  ajax:           Ember.inject.service(),
  store:          Ember.inject.service(),
  session:        Ember.inject.service(),
  sessionAccount: Ember.inject.service('session-account'),
  flashMessages:  Ember.inject.service(),
  trail:          null,

  displayError:      false,
  adventureTypeIcon: Ember.computed('model.adventure_type', function() {
    return this.get('model.adventure_type').dasherize();
  }),
  adventureType:     Ember.computed('model.adventure_type', function() {
    return this.get('model.adventure_type').replace('_', ' ').capitalize();
  }),
  canAttend:         Ember.computed('model.is_owner', 'model.start_time', function() {
    let startTime = this.get('model.start_time');

    return !this.get('model.is_owner') && !this.get('model.canceled') && moment(startTime).isAfter();
  }),
  isYa:              Ember.computed('model.has_responded', 'model.is_attending', function() {
    return this.get('model.has_responded') && this.get('model.is_attending');
  }),
  isNa:              Ember.computed('model.has_responded', 'model.is_attending', function() {
    return this.get('model.has_responded') && !this.get('model.is_attending');
  }),

  trailUrl: Ember.computed('model.trail_id', function() {
    return `${config.APP.TRAIL_STATUS_URL}/trails/${this.get('model.trail_id')}`;
  }),

  trailStatus: Ember.computed('trail.status', 'trail.statusText', function() {
    let status = this.get('trail.status');

    if(status) {
      return this.get(`trail.${status.toLowerCase() + 'Text'}`) || status;
    } else {
      return '';
    }
  }),

  trailStatusIcon: Ember.computed('trail.status', function() {
    let status = this.get('trail.status');

    if(status) {
      return `/img/trail-status/${status}.svg`;
    } else {
      return '';
    }
  }),

  getTrail: function() {
    let ajax = this.get('ajax');
    let self = this;

    if(this.get('model.trail_id')) {
      ajax.request(`${config.APP.TRAIL_STATUS_URL}/api/trails/${this.get('model.trail_id')}`, {
        type: 'GET'
      }).then((data) => {
        self.set('trail', data);
      });
    }
  }.observes('model.trail_id'),

  actions: {
    ya() {
      const flashMessages = this.get('flashMessages');
      let self = this;

      if(self.get('session.isAuthenticated')) {
        var id = self.get('model.invite.id');
        var invite = self.get('store').peekRecord('invite', id);
        let adventure = self.get('model');

        if(!invite) {
          invite = self.get('store')
              .createRecord('invite', {adventure_id: adventure.id});
        }

        invite.accept()
            .then(function() {
              flashMessages.success("You're attending " + self.get('model.name'));

              adventure.reload();

              if(typeof Abba !== 'undefined') {
                Abba('Ya Na Buttons - 2').complete();
              }
            }).catch(function(error) {
          _.each(error.errors, function(e) {
            flashMessages.alert(e.detail);
          });
        });
      } else {
        this.transitionToRoute('login');
      }
    },
    na() {
      const flashMessages = this.get('flashMessages');
      let self = this;

      if(this.get('session.isAuthenticated')) {
        var id = self.get('model.invite.id');
        var invite = self.get('store').peekRecord('invite', id);
        let adventure = this.get('model');

        if(!invite) {
          invite = self.get('store')
              .createRecord('invite', {adventure_id: adventure.id});
        }

        invite.reject()
            .then(function() {
              flashMessages.success("You're not attending " + self.get('model.name'));

              adventure.reload();

              if(typeof Abba !== 'undefined') {
                Abba('Ya Na Buttons - 2').complete();
              }
            }).catch(function(error) {
          _.each(error.errors, function(e) {
            flashMessages.alert(e.detail);
          });
        });
      } else {
        this.transitionToRoute('login');
      }
    }
  }
});
