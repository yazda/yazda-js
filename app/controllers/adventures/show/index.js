import Ember from 'ember';
import moment from 'moment';
import AdventureMixin from 'yazda-js/mixins/adventure';

export default Ember.Controller.extend(AdventureMixin, {
  store:          Ember.inject.service(),
  session:        Ember.inject.service(),
  sessionAccount: Ember.inject.service('session-account'),
  flashMessages:  Ember.inject.service(),

  canAttend:         Ember.computed('model.is_owner', 'model.start_time', function() {
    let startTime = this.get('model.start_time');

    return !this.get('model.is_owner') && !this.get('model.canceled') && moment(startTime).isAfter();
  }),
  skillLevel: Ember.computed('model.skill_level', function() {
    let skillLevel = this.get('model.skill_level');

    switch(skillLevel) {
      case 1:
        return 'easiest';
      case 2:
        return 'easy';
      case 3:
        if('mountain_biking' === this.get('model.adventure_type')) {
          return 'intermediate';
        }

        return 'medium';
      case 4:
        return 'hard';
      case 5:
        return 'hardest';
      default:
        return '';
    }
  }),

  adventureTypeDashed: Ember.computed('model.adventure_type', function() {
    return this.get('model.adventure_type').replace('_', '-');
  })
});
