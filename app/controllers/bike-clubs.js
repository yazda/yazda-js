import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  metrics: Ember.inject.service(),

  actions: {
    open(url) {
      this.get('metrics').trackEvent({'event': 'Lead'});

      if(typeof Abba !== 'undefined') {
        Abba('Registration Bullets - 1').complete();
      }
      window.open(url, '_blank');
    }
  }
});
