import Ember from 'ember';
import moment from 'moment';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  metrics: Ember.inject.service(),

  queryParams: ['page', 'page_size', 'tag'],

  page: 1,
  page_size: 10,

  adventuresGrouped:   Ember.computed('model.adventures.[]', function() {
    let adventures = this.get('model.adventures');
    let currentDate = moment();

    let grouped = adventures.reduce((ret, val) => {
      let date = moment(val.get('start_time'));
      let startTime;

      if(currentDate > date) {
        startTime = currentDate;
      } else {
        startTime = date;
      }

      let adventuresObj = ret.find((value) => {
        return value.key === startTime.format('MMMM Do YYYY');
      });

      if(!adventuresObj) {
        adventuresObj = {
          key:        startTime.format('MMMM Do YYYY'),
          title:      startTime.format('dddd, MMMM Do'),
          adventures: []
        };

        ret.push(adventuresObj);
      }

      if(!adventuresObj.adventures.find(obj => {
            return obj.id === val.id;
          })) {
        adventuresObj.adventures.push(val);
      }

      return ret;
    }, []);

    return grouped;
  }),

  actions: {
    open(url) {
      this.get('metrics').trackEvent({'event': 'Lead'});

      if(typeof Abba !== 'undefined') {
        Abba('Registration Bullets - 1').complete();
      }
      window.open(url, '_blank');
    }
  }
});
