import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    save(clubId) {
      this.transitionToRoute('clubs.show', clubId);
    }
  }
});
