import Ember from 'ember';

export default Ember.Controller.extend({
  geolocation: Ember.inject.service(),

  queryParams: ['q', 'tag', 'lat', 'lon', 'distance'],
  q:           '',
  search:      '',
  tag:         '',
  distance:    100,

  init() {
    let self = this;

    this
        .get('geolocation')
        .getLocation()
        .then(function(geoObject) {
          self.set('lat', geoObject.coords.latitude);
          self.set('lon', geoObject.coords.longitude);
        });
  },

  watchSearch: function() {
    Ember.run.debounce(this, this.runSearch, 750);
  }.observes("search"),

  runSearch: function() {
    let search = this.get('search');
    let re = /^#(\w+)/;
    let results = re.exec(search);

    if(!results) {
      this.set('q', search);
      this.set('tag', '');
    } else {
      this.set('tag', results[1]);
      this.set('q', '');
    }
  }
});
