import Ember from 'ember';
import $ from 'jquery';

export default Ember.Controller.extend({
  init() {
    $(window).scroll(function() {
      if($(this).scrollTop() > 60) {
        // replace main header with fixed nav
        $('.logged-out').removeClass('main-nav-top').addClass('main-nav');
      } else {
        $('.logged-out').removeClass('main-nav').addClass('main-nav-top');
      }
    });
  },
  actions:  {
    transitionToLoginRoute() {
      this.transitionToRoute('login');
    }
  }
});
