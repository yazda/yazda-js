import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Controller.extend({
  ajax:        service(),
  queryParams: ['reset_password_token'],

  actions: {
    resetPassword() {
      let self = this;
      let ajax = this.get('ajax');

      return ajax.request('/reset_password', {
        type:     'PUT',
        dataType: 'text',
        data:     {
          password:              this.get('password'),
          password_confirmation: this.get('password_confirmation'),
          reset_password_token:  this.get('reset_password_token')
        }
      }).then(() => {
        self.transitionToRoute('login');
      });
    }
  }
});
