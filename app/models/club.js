import DS from 'ember-data';

export default DS.Model.extend({
  name:                    DS.attr(),
  description:             DS.attr(),
  html_description:        DS.attr(),
  profile_image_url:       DS.attr(),
  profile_image_thumb_url: DS.attr(),
  banner:                  DS.attr(),
  avatar:                  DS.attr(),
  banner_image_url:        DS.attr(),
  banner_image_thumb_url:  DS.attr(),
  website:                 DS.attr(),
  contact_email:           DS.attr(),
  waiver_url:              DS.attr(),
  waiver:                  DS.attr(),
  location:                DS.attr(),
  lat:                     DS.attr(),
  lon:                     DS.attr(),
  created_at:              DS.attr(),
  updated_at:              DS.attr(),
  member_count:            DS.attr(),
  adventures_count:        DS.attr(),
  is_member:               DS.attr(),
  role:                    DS.attr(),
  adventure_invite:        DS.attr(),
  chat:                    DS.attr(),

  join() {
    this.setProperties({
      _updateEndpoint: 'join',
      _updateVerb:     'POST'
    });

    let promise = this.save();
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  },
  leave() {
    this.setProperties({
      _updateEndpoint: 'leave',
      _updateVerb:     'POST'
    });

    let promise = this.save();
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  },
  uploadBanner() {
    this.setProperties({
      _updateEndpoint: 'update_banner_image',
      _updateVerb:     'POST'
    });

    let promise = this.save();
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  },
  uploadAvatar() {
    this.setProperties({
      _updateEndpoint: 'update_profile_image',
      _updateVerb:     'POST'
    });

    let promise = this.save();
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  },
  uploadWaiver() {
    this.setProperties({
      _updateEndpoint: 'waiver',
      _updateVerb:     'POST'
    });

    let promise = this.save();
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  },
  updateNotification() {
    this.setProperties({
      _updateEndpoint: 'update_notification',
      _updateVerb: 'PATCH'
    });

    let promise = this.save();
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  }
});
