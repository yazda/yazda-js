import User from './register';
import DS from 'ember-data';

export default User.extend({
  location:                DS.attr(),
  profile_image_url:       DS.attr(),
  profile_image_thumb_url: DS.attr(),
  banner_image_url:        DS.attr(),
  banner_image_thumb_url:  DS.attr(),
  description:             DS.attr(),
  html_description:        DS.attr(),
  sign_in_count:           DS.attr(),
  adventures_count:        DS.attr(),
  following_count:         DS.attr(),
  followers_count:         DS.attr(),
  created_at:              DS.attr(),
  created_at_epoch:        DS.attr(),
  updated_at:              DS.attr(),
  updated_at_epoch:        DS.attr(),
  following:               DS.attr(),
  request_sent:            DS.attr(),
  is_adventure_leader:     DS.attr(),
  clubs:                   DS.attr()
});
