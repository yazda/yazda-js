import DS from 'ember-data';

export default DS.Model.extend({
  adventure:    DS.attr(),
  adventure_id: DS.attr(),
  user:         DS.attr(),
  invited_by:   DS.attr(),

  accept() {
    this.setProperties({
      _updateEndpoint: 'accept',
      _updateVerb:     'PUT'
    });

    let promise = this.save({adapterOptions: {adventure_id: this.get('adventure.id')}});
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  },
  reject() {
    this.setProperties({
      _updateEndpoint: 'reject',
      _updateVerb:     'PUT'
    });

    let promise = this.save({adapterOptions: {adventure_id: this.get('adventure.id')}});
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  }
});
