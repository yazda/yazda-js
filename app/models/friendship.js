import DS from 'ember-data';

export default DS.Model.extend({
  status: DS.attr('string'),
  user_id: DS.attr(),
  user:   DS.attr(),
  friend: DS.attr(),

  accept() {
    this.set('_updateEndpoint', 'accept');

    let promise = this.save();
    promise.finally(() => this.set('_updateEndpoint', null));

    return promise;
  },
  reject() {
    this.set('_updateEndpoint', 'reject');

    let promise = this.save();
    promise.finally(() => this.set('_updateEndpoint', null));

    return promise;
  }
});
