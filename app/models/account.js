import User from './user';
import DS from 'ember-data';

export default User.extend({
  views_mountain_biking: DS.attr(),
  views_biking:          DS.attr(),
  views_hiking:          DS.attr(),
  adventure_edit:        DS.attr(),
  adventure_cancel:      DS.attr(),
  adventure_invite:      DS.attr(),
  adventure_joined:      DS.attr(),
  chat:                  DS.attr(),
  friend:                DS.attr(),
  avatar:                DS.attr(),
  banner:                DS.attr(),
  gender:                DS.attr(),
  birthday:              DS.attr(),
  phone:                 DS.attr(),
  postSettings() {
    this.set('_updateEndpoint', 'settings');

    let promise = this.save();
    promise.finally(() => this.set('_updateEndpoint', null));

    return promise;
  },
  uploadBanner() {
    this.setProperties({
      _updateEndpoint: 'update_banner_image',
      _updateVerb:     'POST'
    });

    let promise = this.save();
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  },
  uploadAvatar() {
    this.setProperties({
      _updateEndpoint: 'update_profile_image',
      _updateVerb:     'POST'
    });

    let promise = this.save();
    promise.finally(() => {
      this.setProperties({
        _updateEndpoint: null,
        _updateVerb:     null
      });
    });

    return promise;
  }
});
