import User from './user';
import DS from 'ember-data';

export default User.extend({
  following:           DS.attr(),
  request_sent:        DS.attr(),
  is_adventure_leader: DS.attr()
});
