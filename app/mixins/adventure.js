import Ember from 'ember';
import moment from 'moment';

export default Ember.Mixin.create({
  startTime:      Ember.computed('model.start_time', function() {
    let time = this.get('model.start_time');

    if(moment(time).isBefore()) {
      return 'Now';
    } else {
      return moment(time).format('h:mm A');
    }
  }),
  privacyText:    Ember.computed('model.privacy', function() {
    let privacy = this.get('model.privacy');

    switch(privacy) {
      case 'private_adventure':
        return 'Private';
      case 'link':
        return 'Viewable with Link';
      default:
        return 'Open';
    }
  }),
  isPrivate:      Ember.computed('model.privacy', function() {
    return this.get('model.privacy') !== 'public_adventure';
  }),
  is24Hours:      Ember.computed('model.duration_in_minutes', function() {
    return this.get('model.duration_in_minutes') > 1439;
  }),
  adventurerText: Ember.computed('model.attendings_count', function() {
    return `${this.get('model.attendings_count') } Adventurers`;
  }),
  spotsLeft:      Ember.computed('model.reservation_limit', 'model.attendings_count', function() {
    let limit = this.get('model.reservation_limit');
    let attendings = this.get('model.attendings_count');

    if(limit && limit > attendings) {
      return limit - attendings;
    } else {
      return 'No';
    }
  })
});
