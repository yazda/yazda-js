import Ember from 'ember';

export default Ember.Mixin.create({
  page:       1,
  page_count: 0,
  page_size:  10,
  total:      0,

  actions:             {
    nextPage() {
      let currentPage = this.get('page');

      if(currentPage < this.get('page_count')) {
        this.set('page', currentPage + 1);
      }
    },
    previousPage() {
      let currentPage = this.get('page');

      if(currentPage > 1) {
        this.set('page', currentPage - 1);
      }
    },
    selectPage(page) {
      this.set('page', page);
    }
  },
  hasNextPage:         Ember.computed('page', 'page_count', function() {
    return this.get('page') < this.get('page_count');
  }),
  hasPreviousPage:     Ember.computed('page', function() {
    return this.get('page') > 1;
  }),
  pageObserver:        Ember.observer('page', 'page_size', function() {
    this.set('loading', true);
    Ember.run.once(this, 'getModels');
  })
});
