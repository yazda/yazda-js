import Ember from 'ember';
const {service} = Ember.inject;

export default Ember.Mixin.create({
  metaTags:       service('meta-tags'),

  afterModel(model) {
    if(typeof this._createHeadTags === 'function') {
      this.set('headTags', this._createHeadTags(model));
    }

    this._super(...arguments);
  }
});
