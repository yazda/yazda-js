import Ember from 'ember';

const {service} = Ember.inject;

export default Ember.Mixin.create({
  cookies: service(),

  getRedirect() {
    let cookie = this.get('cookies');
    let url = document.location.href;
    let regex = new RegExp("[?&]r(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);

    if(!results) {
      return cookie.read('external-redirect_url');
    }
    if(!results[2]) {
      return cookie.read('external-redirect_url');
    }

    let decoded = decodeURIComponent(results[2].replace(/\+/g, " "));
    let redirect = decoded.indexOf('redirect_uri') !== -1 ? decoded : null;

    cookie.write('external-redirect_url', redirect);

    let rtn = url.split("?")[0],
        param,
        params_arr = [],
        queryString = (url.indexOf("?") !== -1) ? url.split("?")[1] : "";
    if(queryString !== "") {
      params_arr = queryString.split("&");
      for(var i = params_arr.length - 1; i >= 0; i -= 1) {
        param = params_arr[i].split("=")[0];
        if(param === 'r') {
          params_arr.splice(i, 1);
        }
      }
      rtn = rtn + "?" + params_arr.join("&");
    }

    return cookie.read('external-redirect_url') || redirect;
  },

  redirect() {
    let cookie = this.get('cookies');
    let redirectUri = this.getRedirect();
    let isAuthenticated = this.get('session').get('isAuthenticated');

    if(redirectUri && isAuthenticated) {
      let accessToken = this.get('session.data.authenticated.access_token');
      cookie.clear('external-redirect_url');

      return document.location.href = `${redirectUri}&access_token=${accessToken}`;
    }
  },
});
