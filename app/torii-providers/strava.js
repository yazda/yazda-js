import {configurable} from 'torii/configuration';
import Oauth2 from 'torii/providers/oauth2-code';

export default Oauth2.extend({
  name:    'strava',
  baseUrl: 'https://www.strava.com/api/v3/oauth/authorize',

  // Additional url params that this provider requires
  requiredUrlParams: ['client_id', 'redirectUri', 'response_type'],

  responseParams: ['code'],

  scope:        configurable('scope', ''),

  display: 'popup',
  redirectUri: configurable('redirectUri', function(){
    // A hack that allows redirectUri to be configurable
    // but default to the superclass
    return this._super();
  }),

  open: function() {
    return this._super().then(function(authData){
      if (authData.authorizationCode && authData.authorizationCode === '200') {
        // indication that the user hit 'cancel', not 'ok'
        throw new Error('User canceled authorization');
      }

      return authData;
    });
  },
  fetch: function(data) {
    return data;
  }
});
