export default {
  small:  '(max-width: 39.9375em)',
  medium:  '(min-width: 40em) and (max-width: 63.9375em)',
  large: '(min-width: 64em)'
};
