import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import config from '../config/environment';

export default DS.RESTAdapter.extend(DataAdapterMixin, {
  authorizer: 'authorizer:application',
  namespace:  'v1',
  host:       config.host,
  shouldReloadRecord: function() {
    return false;
  },

  shouldReloadAll: function() {
    return false;
  },

  shouldBackgroundReloadRecord() {
    return true;
  },

  shouldBackgroundReloadAll() {
    return true;
  }
});
