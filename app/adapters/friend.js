import ApplicationAdapter from './application';
import UrlTemplates from "ember-data-url-templates";

export default ApplicationAdapter.extend(UrlTemplates, {
  urlTemplate:        '{+host}/{+namespace}/users{/userId}/{pathForType}',
  urlSegments: {
    userId(type, id, snapshot, query) {
      // we're extracting the endpoint from the query object...
      let ep = query.userId;

      // ... and we delete it, so that the actual query does not contain
      // a useless "endpoint"
      delete query.userId;

      return ep;
    }
  }
});
