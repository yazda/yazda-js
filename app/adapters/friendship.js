import ApplicationAdapter from './application';
import UrlTemplates from "ember-data-url-templates";

export default ApplicationAdapter.extend(UrlTemplates, {
  queryUrlTemplate:        '{+host}/{+namespace}/friendships/{endpoint}',
  updateRecordUrlTemplate: '{+host}/{+namespace}/friendships/{id}{/updateEndpoint}',

  urlSegments: {
    endpoint(type, id, snapshot, query) {
      // we're extracting the endpoint from the query object...
      let ep = query.endpoint;

      // ... and we delete it, so that the actual query does not contain
      // a useless "endpoint"
      delete query.endpoint;

      return ep;
    },
    updateEndpoint(type, id, snapshot) {
      return snapshot.record.get('_updateEndpoint');
    }
  }
});
