import ApplicationAdapter from './application';
import UrlTemplates from "ember-data-url-templates";

export default ApplicationAdapter.extend(UrlTemplates, {
  queryUrlTemplate:        '{+host}/{+namespace}/invites/{endpoint}',
  createRecordUrlTemplate: '{+host}/{+namespace}/invites{/updateEndpoint}',
  updateRecordUrlTemplate: '{+host}/{+namespace}/invites{/updateEndpoint}',

  urlSegments: {
    endpoint(type, id, snapshot, query) {
      // we're extracting the endpoint from the query object...
      let ep = query.endpoint;

      // ... and we delete it, so that the actual query does not contain
      // a useless "endpoint"
      delete query.endpoint;

      return ep;
    },
    updateEndpoint(type, id, snapshot) {
      return snapshot.record.get('_updateEndpoint');
    }
  },

  createRecord: function(store, type, snapshot) {
    var data = {};
    var serializer = store.serializerFor(type.modelName);
    var url = this.buildURL(type.modelName, null, snapshot, 'createRecord');
    var verb = snapshot.record.get('_updateVerb') || "PUT";

    serializer.serializeIntoHash(data, type, snapshot, { includeId: true });

    return this.ajax(url, verb, { data: data });
  }
});
