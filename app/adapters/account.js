import ApplicationAdapter from './application';
import UrlTemplates from "ember-data-url-templates";

export default ApplicationAdapter.extend(UrlTemplates, {
  updateRecordUrlTemplate: '{+host}/{+namespace}/account{/updateEndpoint}',

  urlSegments: {
    updateEndpoint(type, id, snapshot) {
      return snapshot.record.get('_updateEndpoint');
    }
  },
  pathForType(modelName) {
    return modelName;
  },

  updateRecord(store, type, snapshot) {
    var data = {};
    var serializer = store.serializerFor(type.modelName);
    var url = this.buildURL(type.modelName, null, snapshot, 'updateRecord');
    var verb = snapshot.record.get('_updateVerb') || "PUT";

    serializer.serializeIntoHash(data, type, snapshot, { includeId: true });

    return this.ajax(url, verb, { data: data });
  }
});
