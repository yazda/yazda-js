/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'yazda-js',
    environment:  environment,
    baseURL:      '/',
    locationType: 'auto',
    flashMessageDefaults: {
      // flash message defaults
      timeout: 4000
    },
    EmberENV:     {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
      'TRAIL_STATUS_URL': 'https://trail-status.com'
    },

    contentSecurityPolicy:  {
      'style-src': "'self' 'unsafe-inline'"
    },
    browserify:             {
      tests: true
    },
    torii:                  {
      providers: {
        'facebook-oauth2': {
          apiKey: '980385968648785',
          scope: 'email,public_profile,user_friends'
        },
        'strava':          {
          clientId:     '10708',
          responseType: 'code'
        }
      }
    },
    'ember-simple-auth':    {
      baseURL: 'http://dev.yazdaapp.com'
    },
    bugsnag:                {
      apiKey:              '43dcc8a866388325f5564a2dd0b7aa8d',
      notifyReleaseStages: ['production']
    },
    metricsAdapters: [
      {
        name: 'GoogleTagManager',
        environments: ['production'],
        config: {
          id: 'GTM-NVTQ5P2',
          options: {
            dataLayer: [{'event':'loaded'}]
          }
        }
      },
      {
        name: 'FacebookPixel',
        environments: ['production'],
        config: {
          id: '1041067409260335'
        }
      },
    ]
  };

  ENV['place-autocomplete'] = {
    exclude: true
  };

  ENV['g-map'] = {
    libraries: ['places', 'geometry'],
    key:       'AIzaSyDKuGzOr6O92ch6iazE7SjyH5KN_JxOsx0',
    protocol:  'https'
  };

  if(environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    ENV.APP.LOG_TRANSITIONS = true;
    ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.APP.BRANCH_ID = 'key_test_bmlIK0C3ahGT5n7Xm5jpypkbwAacXSma';

    ENV.host = ENV['ember-simple-auth'].baseURL = 'http://dev.yazdaapp.com';
    ENV.socket = 'ws://192.168.99.100:8080';

    ENV.ABBA = '//localhost:5000/v1/abba.js';
  }

  if(environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;
    ENV.socket = 'ws://localhost:8080';
    ENV.APP.rootElement = '#ember-testing';
  }

  if(environment === 'production') {
    ENV.APP.BRANCH_ID = 'key_live_fomHN5t1mbHUXc62i5mfDngisCmk8Tnt';

    ENV.torii.providers['facebook-oauth2'].apiKey = '980360288651353';
    ENV.host = ENV['ember-simple-auth'].baseURL = 'https://api.yazdaapp.com';
    ENV.socket = 'wss://chat.yazdaapp.com';
    ENV.ABBA = '//agile-mountain-57666.herokuapp.com/v1/abba.js';
  }

  return ENV;
};
