import { moduleFor, test } from 'ember-qunit';

moduleFor('service:meta-tags', 'Unit | Service | meta tags', {
  // Specify the other units that are required for this test.
  // needs: ['service:foo']
});

// Replace this with your real tests.
test('it returns a meta tag', function(assert) {
  let service = this.subject();
  assert.equal(service.createTag('test','content'), { type: 'meta', tagId: 'test', attrs: { property: 'test', content: 'content'}});
});

test('it throws an error', function(assert) {
  let service = this.subject();
  assert.equal(service.createTag(), undefined);
});
//TODO
