import {moduleForComponent, test} from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import moment from 'moment';
import Ember from 'ember';

const sessionAccountServiceStub = Ember.Service.extend({
  account: {
    id: 1
  },

  session: {
    data: {
      authenticated: {
        access_token: 'test'
      }
    }
  },

  loadCurrentUser() {
    return true;
  }
});

moduleForComponent('chat-item', 'Integration | Component | chat item', {
  integration: true,
  beforeEach:  function() {
    this.register('service:session-account', sessionAccountServiceStub);
    this.inject.service('session-account', {as: 'sessionAccount'});
  }
});

const message = {
  user:    {
    profile_image_url: 'http://test.com',
    name:              'Test',
    id:                1
  },
  msg:     'here is a test message',
  sent_at: moment(),
};

const leftMessage = {
  user:    {
    profile_image_url: 'http://test.com',
    name:              'Test',
    id:                2
  },
  msg:     'here is a test message',
  sent_at: moment(),
};

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });
  this.set('message', message);
  this.render(hbs`{{chat-item message=message}}`);

  assert.equal(this.$('img').attr('src'), message.user.profile_image_url);
  assert.equal(this.$('.chat-text').text(), message.msg);
  assert.equal(this.$('.chat-detail strong').text(), message.user.name);
  assert.ok(this.$('.right').length);

  // Switches sides
  this.set('message', leftMessage);
  assert.ok(this.$('.left').length);
});

