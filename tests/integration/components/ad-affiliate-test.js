import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('ad-affiliate', 'Integration | Component | ad affiliate', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{ad-affiliate}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#ad-affiliate}}
      template block text
    {{/ad-affiliate}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
//TODO needs testing
