import {moduleForComponent, test} from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('user-show-image', 'Integration | Component | user show image', {
  integration: true
});

const user = {
  profile_image_url:   'https://yazdaapp.com/users/1/profile_image',
  name:                'Test',
  id:                  1,
  is_adventure_leader: false
};

test('it renders', function(assert) {
  this.set('user', user);
  this.render(hbs`{{user-show-image user=user}}`);

  // TODO test routes
  // assert.equal(this.$('.thumb-show').attr('href'), `/users/${user.id}`);
  assert.equal(this.$('.thumb-show').attr('title'), user.name);
  assert.equal(this.$('.user-image-name').text(), user.name);
  assert.equal(this.$('img').attr('src'), user.profile_image_url);
  assert.notOk(this.$('.badge-adventure-leader').length);
});

test('it renders adventure leader badge', function(assert) {
  this.set('user', user);
  this.render(hbs`{{user-show-image user=user}}`);

  assert.notOk(this.$('.badge-adventure-leader').length);

  this.set('user.is_adventure_leader', true);
  assert.ok(this.$('.badge-adventure-leader').length);
});
