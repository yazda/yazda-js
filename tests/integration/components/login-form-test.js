import {moduleForComponent, test} from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

moduleForComponent('login-form', 'Integration | Component | login form', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{login-form}}`);

  assert.equal(this.$('.strava').text().trim(), 'Sign in with Strava');
  assert.equal(this.$('.facebook').text().trim(), 'Sign in with Facebook');
  assert.ok(this.$('input[placeholder="Email"]').length);
  assert.ok(this.$('input[placeholder="Password"]').length);
  assert.ok(this.$('input[type="submit"]').length);
  assert.ok(this.$('.menu').text().trim().indexOf('Not a member yet?') !== -1);
  assert.ok(this.$('.menu').text().trim().indexOf('Forgot password?') !== -1);
  assert.equal(this.$('.disclaimer').text().trim(), 'By signing in you agree' +
      ' to our Terms Of Use and Privacy Policy');
});

test('displays an error message', function(assert) {
  let message = 'error!!!!';

  this.render(hbs`{{login-form}}`);

  assert.notOk(this.$('.alert').length);

  this.set('errorMessage', message);
  this.render(hbs`{{login-form errorMessage=errorMessage}}`);

  assert.equal(this.$('.alert').text().trim(), message);
});

test('logs in with Strava', function(assert) {
  const session = Ember.Service.extend({
    authenticate(authenticator, provider) {
      assert.equal('authenticator:torii', authenticator);
      assert.equal('strava', provider);
    }
  });

  this.register('service:session', session);
  this.inject.service('session', {as: 'session'});
  this.render(hbs`{{login-form}}`);

  this.$('.strava').click();
});

test('logs in with Facebook', function(assert) {
  const session = Ember.Service.extend({
    authenticate(authenticator, provider) {
      assert.equal('authenticator:torii', authenticator);
      assert.equal('facebook', provider);
    }
  });

  this.register('service:session', session);
  this.inject.service('session', {as: 'session'});
  this.render(hbs`{{login-form}}`);

  this.$('.facebook').click();
});

test('logs in with email', function(assert) {
  const inputEmail = 'test@test.com';
  const inputPassword = 'password';
  const session = Ember.Service.extend({
    authenticate(authenticator, email, password) {
      assert.equal('authenticator:oauth2', authenticator);
      assert.equal(inputEmail, email);
      assert.equal(inputPassword, password);

      return {
        catch() {}
      };
    }
  });

  this.register('service:session', session);
  this.inject.service('session', {as: 'session'});
  this.render(hbs`{{login-form}}`);

  this.$('input[type="email"]').val(inputEmail);
  this.$('input[type="email"]').change();
  this.$('input[type="password"]').val(inputPassword);
  this.$('input[type="password"]').change();
  this.$('form').submit();
});
