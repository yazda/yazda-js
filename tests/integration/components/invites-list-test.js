import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('invites-list', 'Integration | Component | invites list', {
  integration: true
});

test('it does not display if loading', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{invites-list}}`);

  assert.equal(this.$().text().trim(), '');
});
//TODO needs testing
