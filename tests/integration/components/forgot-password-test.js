import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('forgot-password', 'Integration | Component | forgot password', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });


  this.render(hbs`{{forgot-password}}`);

  assert.equal(this.$().text().trim(), '');
});
//TODO needs testing
