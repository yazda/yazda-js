//TODO needs testing
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('adventure-type', 'Integration | Component | adventure type', {
  integration: true
});

test('biking', function(assert) {
  this.render(hbs`{{adventure-type adventureType='biking'}}`);

  assert.equal(this.$('i').attr('class'), 'icon-biking');
  assert.equal(this.$('.adventure-type').text().trim(), 'Biking');

  this.render(hbs`
    {{#adventure-type adventureType='biking'}}
      template block text
    {{/adventure-type}}
  `);

  assert.ok(this.$().text().indexOf('template block text') !== -1);
});

test('mountain_biking', function(assert) {
  this.render(hbs`{{adventure-type adventureType='mountain_biking'}}`);

  assert.equal(this.$('i').attr('class'), 'icon-mountain-biking');
  assert.equal(this.$('.adventure-type').text().trim(), 'Mountain biking');

  this.render(hbs`
    {{#adventure-type adventureType='mountain_biking'}}
      template block text
    {{/adventure-type}}
  `);

  assert.ok(this.$().text().indexOf('template block text') !== -1);
});

test('hiking', function(assert) {
  this.render(hbs`{{adventure-type adventureType='hiking'}}`);

  assert.equal(this.$('i').attr('class'), 'icon-hiking');
  assert.equal(this.$('.adventure-type').text().trim(), 'Hiking');

  this.render(hbs`
    {{#adventure-type adventureType='hiking'}}
      template block text
    {{/adventure-type}}
  `);

  assert.ok(this.$().text().indexOf('template block text') !== -1);
});

test('running', function(assert) {
  this.render(hbs`{{adventure-type adventureType='running'}}`);

  assert.equal(this.$('i').attr('class'), 'icon-running');
  assert.equal(this.$('.adventure-type').text().trim(), 'Running');

  this.render(hbs`
    {{#adventure-type adventureType='running'}}
      template block text
    {{/adventure-type}}
  `);

  assert.ok(this.$().text().indexOf('template block text') !== -1);
});

test('trail_running', function(assert) {
  this.render(hbs`{{adventure-type adventureType='trail_running'}}`);

  assert.equal(this.$('i').attr('class'), 'icon-trail-running');
  assert.equal(this.$('.adventure-type').text().trim(), 'Trail running');

  this.render(hbs`
    {{#adventure-type adventureType='trail_running'}}
      template block text
    {{/adventure-type}}
  `);

  assert.ok(this.$().text().indexOf('template block text') !== -1);
});
