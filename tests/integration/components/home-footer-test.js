import {moduleForComponent, test} from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import moment from 'moment';

moduleForComponent('home-footer', 'Integration | Component | home footer', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{home-footer}}`);

  assert.equal(this.$('.footer-heading').text().trim(), 'Find Us On');
  assert.equal(this.$('.menu a:last').text().trim(), 'Terms of use');
  assert.equal(this.$('.menu a:first').text().trim(), 'Privacy policy');
  assert.ok(this.$('.ion-social-facebook').length);
  assert.ok(this.$('.ion-social-twitter').length);
  assert.ok(this.$('.ion-social-youtube').length);
  assert.ok(this.$('.ion-social-instagram').length);
  assert.ok(this.$('.ion-email').length);
});

test('it sets the date', function(assert) {
  let date = moment();

  this.render(hbs`{{home-footer}}`);

  assert.ok(this.$('.copyright').text().indexOf(date.format('YYYY')) !== -1);
});
