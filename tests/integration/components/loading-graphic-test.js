import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('loading-graphic', 'Integration | Component | loading graphic', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{loading-graphic}}`);

  assert.equal(this.$().text().trim(), 'Loading...');
  assert.ok(this.$('#loader').length);
  assert.ok(this.$('.spinning-gear-1'));
});
