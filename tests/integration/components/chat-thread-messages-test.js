import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('chat-thread-messages', 'Integration | Component | chat thread messages', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{chat-thread-messages}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#chat-thread-messages}}
      template block text
    {{/chat-thread-messages}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
//TODO needs testing
