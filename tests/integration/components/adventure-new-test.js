import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('adventure-new', 'Integration | Component | adventure new', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{adventure-new}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#adventure-new}}
      template block text
    {{/adventure-new}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
//TODO needs testing
