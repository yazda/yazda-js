import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('potential-friends', 'Integration | Component | potential friends', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{potential-friends}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#potential-friends}}
      template block text
    {{/potential-friends}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
//TODO needs testing
