import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('adventures-list', 'Integration | Component | adventures list', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{adventures-list}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#adventures-list}}
      template block text
    {{/adventures-list}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
//TODO needs testing
