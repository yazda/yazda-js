import {moduleForComponent, test} from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

const routingStub = Ember.Service.extend({
  currentRouteName: 'index',
  generateURL() {
  }
});

const sessionStub = Ember.Service.extend({
  isAuthenticated: false,
  authorize() {
  }
});

moduleForComponent('main-navigation', 'Integration | Component | main navigation', {
  integration: true,

  beforeEach: function() {
    this.register('service:-routing', routingStub);
    this.register('service:session', sessionStub);
    // Calling inject puts the service instance in the test's context,
    // making it accessible as "locationService" within each test
    this.inject.service('-routing', {as: 'routing'});
    this.inject.service('session', {as: 'session'});
  }
});

test('default navigation logged in', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('session.isAuthenticated', true);
  this.render(hbs`{{main-navigation}}`);

  assert.strictEqual(this.$('.signin-btn').length, 0);
});


test('default navigation not logged in', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{main-navigation}}`);

  this.$('.signin-btn[href="/register"]').each(function() {
    assert.strictEqual($(this).text(), 'Register', 'register button');
  });

  this.$('.signin-btn[href="/sign_in"]').each(function() {
    assert.strictEqual($(this).text(), 'Sign In', 'sign in button');
  });

  assert.strictEqual(this.$('.signin-btn').length, 2);
});

test('clubs.show route', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('routing.currentRouteName', 'clubs.show');
  this.set('session.isAuthenticated', true);
  this.render(hbs`{{main-navigation}}`);

  assert.strictEqual(this.$('.icon-profile').length, 0, 'view profile');
  assert.strictEqual(this.$('.edit-profile').length, 0, 'edit profile');
  assert.strictEqual(this.$('.icon-club-add').length, 1, 'add club');
  assert.strictEqual(this.$('.cancel').length, 0, 'cancel');
  assert.strictEqual(this.$('.icon-adventure-add').length, 1, 'add adventure');
});

test('clubs.edit route', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('routing.currentRouteName', 'clubs.edit');
  this.set('session.isAuthenticated', true);
  this.render(hbs`{{main-navigation}}`);

  assert.strictEqual(this.$('.icon-profile').length, 0, 'view profile');
  assert.strictEqual(this.$('.edit-profile').length, 0, 'edit profile');
  assert.strictEqual(this.$('.icon-club-add').length, 0, 'add club');
  assert.strictEqual(this.$('.cancel').length, 1, 'cancel');
  assert.strictEqual(this.$('.icon-adventure-add').length, 0, 'add adventure');
});

test('adventures.new route', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('routing.currentRouteName', 'adventures.new');
  this.set('session.isAuthenticated', true);
  this.render(hbs`{{main-navigation}}`);

  assert.strictEqual(this.$('.icon-profile').length, 0, 'view profile');
  assert.strictEqual(this.$('.edit-profile').length, 0, 'edit profile');
  assert.strictEqual(this.$('.icon-club-add').length, 0, 'add club');
  assert.strictEqual(this.$('.cancel').length, 1, 'cancel');
  assert.strictEqual(this.$('.icon-adventure-add').length, 0, 'add adventure');
});

test('profile.index route', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('routing.currentRouteName', 'profile.index');
  this.set('session.isAuthenticated', true);
  this.render(hbs`{{main-navigation}}`);

  assert.strictEqual(this.$('.icon-profile').length, 0, 'view profile');
  assert.strictEqual(this.$('.edit-profile').length, 1, 'edit profile');
  assert.strictEqual(this.$('.icon-club-add').length, 0, 'add club');
  assert.strictEqual(this.$('.cancel').length, 0, 'cancel');
  assert.strictEqual(this.$('.icon-adventure-add').length, 1, 'add adventure');
});

test('profile.edit route', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('routing.currentRouteName', 'profile.edit');
  this.set('session.isAuthenticated', true);
  this.render(hbs`{{main-navigation}}`);

  assert.strictEqual(this.$('.icon-profile').length, 0, 'view profile');
  assert.strictEqual(this.$('.edit-profile').length, 0, 'edit profile');
  assert.strictEqual(this.$('.icon-club-add').length, 0, 'add club');
  assert.strictEqual(this.$('.cancel').length, 1, 'cancel');
  assert.strictEqual(this.$('.icon-adventure-add').length, 0, 'add adventure');
});
